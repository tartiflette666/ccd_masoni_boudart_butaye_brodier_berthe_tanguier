-- phpMyAdmin SQL Dump
-- version 4.1.14.8
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Jeu 08 Février 2018 à 15:41
-- Version du serveur :  5.1.73
-- Version de PHP :  7.0.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `berthe18u`
--

-- --------------------------------------------------------

--
-- Structure de la table `avis_ccd`
--

DROP TABLE IF EXISTS `avis_ccd`;
CREATE TABLE IF NOT EXISTS `avis_ccd` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `commentaire` varchar(200) NOT NULL,
  `date` date NOT NULL,
  `id_user` int(5) NOT NULL,
  `id_item` int(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Contenu de la table `avis_ccd`
--

INSERT INTO `avis_ccd` (`id`, `commentaire`, `date`, `id_user`, `id_item`) VALUES
(1, 'Super atelier j''ai pu y réparer ma voiture qui était vraiment très mal en point...', '2018-02-01', 4, 1),
(2, 'Un peu rudimentaire mais tout de même bien pratique', '2017-11-13', 3, 2),
(3, 'Le mécanicien qui m''a aidé était trop mignon ! Je recommande !!', '2017-07-17', 1, 5),
(4, 'Super pratique quand on beaucoup de choses à faire. Merci à toute l''équipe de mettre à disposition autant de matériel !', '2017-12-18', 4, 5),
(5, 'Très propre. J''ai apprécié.', '2017-12-12', 11, 6),
(6, 'Les miroirs sont un peu dérangeants, surtout quand on travaille avec la lampe...', '2018-02-02', 7, 6),
(7, 'J''adore travailler à l''air frais ! Les couchers de soleil sont tellement magnifiques...', '2017-12-20', 7, 3),
(8, 'Je ne recommande pas pour travailler en hiver...', '2018-01-22', 11, 7),
(9, 'J''adore rouler en Bentley !!', '2017-06-19', 3, 8),
(10, 'Cette voiture me rappelle celle de mes grands parents. C''est la classe!!', '2017-09-04', 1, 14),
(11, 'Un peu frisquet par temps de grand froid...', '2017-12-13', 3, 15),
(12, 'Mon chien a adoré le grand coffre!', '2017-11-23', 10, 17),
(13, 'Une voiture de course super classe ! J''ai roulé 150km/h pendant au moins 10km!!', '2017-12-05', 12, 18),
(14, 'La Batmobile. La meilleure. ', '2017-11-22', 2, 19),
(15, 'Ma copine était très impressionnée quand je suis venue la chercher ! Merci à vous !!', '2017-08-17', 9, 19),
(16, 'Mes enfants ont adoré! Mon mari un peu moins...', '2017-08-16', 11, 21),
(17, 'Idéal pour promener belle-maman.', '2017-03-16', 12, 21);

-- --------------------------------------------------------

--
-- Structure de la table `cagnotte_ccd`
--

DROP TABLE IF EXISTS `cagnotte_ccd`;
CREATE TABLE IF NOT EXISTS `cagnotte_ccd` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `token` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Contenu de la table `cagnotte_ccd`
--

INSERT INTO `cagnotte_ccd` (`id`, `token`) VALUES
(1, '9fbd1c17078adb9e'),
(2, 'ef7277864a38bdf2');

-- --------------------------------------------------------

--
-- Structure de la table `categorie_ccd`
--

DROP TABLE IF EXISTS `categorie_ccd`;
CREATE TABLE IF NOT EXISTS `categorie_ccd` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(30) CHARACTER SET utf8 NOT NULL,
  `description` text CHARACTER SET utf8 NOT NULL,
  `image` varchar(200) NOT NULL,
  `token` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `categorie_ccd`
--

INSERT INTO `categorie_ccd` (`id`, `nom`, `description`, `image`, `token`) VALUES
(1, 'Vehicule', 'Tous les véhicules à emprunter !!!', '1.jpg', '3861fc8eaed163d9'),
(2, 'Atelier', 'Des ateliers réservables pour moult réparations.', '2.jpg', '1f69a26015aa68ea');

-- --------------------------------------------------------

--
-- Structure de la table `item_ccd`
--

DROP TABLE IF EXISTS `item_ccd`;
CREATE TABLE IF NOT EXISTS `item_ccd` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(30) CHARACTER SET utf8 NOT NULL,
  `description` text CHARACTER SET utf8 NOT NULL,
  `image` varchar(200) NOT NULL,
  `token` varchar(200) NOT NULL,
  `id_categ` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_categ` (`id_categ`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Contenu de la table `item_ccd`
--

INSERT INTO `item_ccd` (`id`, `nom`, `description`, `image`, `token`, `id_categ`) VALUES
(1, 'Atelier en bois', 'Cet atelier en bois est l''idéal pour réparer votre voiture tout en respirant la belle essence de Cyprès.', '1.jpg', '5421a030276dc02d', 2),
(2, 'Atelier BX023 en brique', 'Rustique, simple et fonctionnel, ce box vous permet de réparer votre véhicule sans vous perturber par son décorum. Un must pour les travaux difficiles !', '2.jpg', 'eb1321b62844242e', 2),
(3, 'Batcave', 'L''atelier qu''il vous faut pour réparer secrètement votre batmobile (fourni sans Albert ni Bruce Wayne). ', '3.jpg', 'b52a4b45cf0e5aff', 2),
(4, 'Atelier BX045', 'Sans lumière mais disposant d''ouvertures au plafond, ce box est à réserver aux opérations les plus simples. Une lampe torche est fournie à l''entrée pour que vous puissiez retrouver les pièces perdues.', '4.jpg', '3ac41f056a68d66e', 2),
(5, 'Atelier du futur', 'Avec cet atelier, vous serez déjà en l''an 3000 !! Grand, bien agencé, ce box accueillera toutes vos voitures cylindriques dernier modèle.', '5.jpg', 'be23ceabbc50e920', 2),
(6, 'Atelier Miroir', 'Le fond de l''atelier est tellement reflechissant qu''on peut se voir dedans. Mr Propre y vient régulièrement. ', '6.jpg', 'ea9064413c491947', 2),
(7, 'Atelier du soleil', 'L''atelier avec la plus belle vue pour pouvoir prendre de splendides photos et immortaliser ses réparations.', '7.jpg', 'dee7072a70097104', 2),
(8, 'Bentley', 'Bentley continentale, couleur gris métalisé, essence, deux portes. Ben t''létait pas au courant ?', '8.jpg', 'eb38b49a79d0a69d', 1),
(9, 'Rolls Royce', 'Rolls Royce oldtimer, 12 places, voiture de 1978, restaurée. Sortez en famille en rolls Royce pour les plus grandes occasions.', '9.jpg', '4dda332b7954582a', 1),
(10, 'Opel', 'Envie de vous déplacer en toute discrétion dans les années 80, cette Opel est faite pour vous.', '10.jpg', 'e0cfa4f0da48166e', 1),
(11, 'Atelier securité', 'Pour effectuer vos réparations sans jamais être importuné, cet atelier propose de multiples volets métalliques insonorisés (ne limitent le propagation du son que fermés).', '11.jpg', '3c36d6575417f357', 2),
(12, 'Atelier multiple', 'Cet atelier permet d''effectuer plusieurs réparations en simultanée. Un must pour les grands bricoleurs.', '12.jpg', '11b9b7abfcc25412', 2),
(13, 'Porshe 911', 'Porshe 911, noire, deux portes. Elegante et distinguée, la Rolls des voitures (juste aprés Rolls). Elle est tellement BELLE que l''on écrit en majusCULES.', '13.jpg', 'c6124fa3b8f83d1b', 1),
(14, 'Fiat 500', 'Fiat 500, Rouge avec son trait central blanc, diesel, deux portes. Petite mais costaude.', '14.jpg', 'b735a666c9f6f456', 1),
(15, 'Rolls Royce Cabriolet', 'Rolls Royce Cabriolet, jaune canari, 2 portes. Tentez votre chance, remportez tous les prix des courses d''il y a 50 ans avec cette voiture.', '15.jpg', 'e94078e9d073447c', 1),
(16, 'BMW 600', 'BMW 600, année 1957-1959, couleur bleu, une porte. Une seule porte mais tellement de place ! Ce serait dommage de ne pas la tester.', '16.jpg', '7a9ad12c5c20b9e4', 1),
(17, 'R4 Renault', 'Renault R4, couleur rouge, 4 portes. Une voiture et un modèle qui n''a pas vieilli.', '17.jpg', '785e1e4f48fcf903', 1),
(18, 'Batmobile (réplique)', 'Batmobile (réplique), couleur noire à bordereau rouge. Idéale pour aller chasser le Joker ou faire un coucou au Pinguoin (Robin non inclus). ', '18.jpg', '281f274e4b920db2', 1),
(19, 'Ferrari rouge', 'La Ferrari, la classique, la connue, la reputée, la pizza regina des voitures. What else ?', '19.jpg', '8c08863c5a155e19', 1),
(20, 'Bus VW', 'Bus volkswagen, couleur vert-olive-pas-tout-a-fait-mure en bas, blanc en haut. Partir en famille sans se préoccuper de l''espace disponible, c''est possible !!', '20.jpg', 'bbd3d5344b12d85d', 1),
(21, 'Charette', 'Charette à bras, couleur bois, pratique et efficace, à locomotion forcée. A noter que les bras ne sont pas fournis avec le véhicule.', '21.jpg', '8b5b6e519e6ebc31', 1),
(22, 'Batmobile (la vraie)', 'N''exigez qu''une batmobile, la seule et l''unique !!! Batmobile véritable construite dans les batiments de Wayne industrie. (ps: par contre, c''est vrai que les répliques sont bien faites)', '22.jpg', '8b2f9b5c8782679e', 1);

-- --------------------------------------------------------

--
-- Structure de la table `options_ccd`
--

DROP TABLE IF EXISTS `options_ccd`;
CREATE TABLE IF NOT EXISTS `options_ccd` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) NOT NULL,
  `description` varchar(200) NOT NULL,
  `quantite` int(5) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Contenu de la table `options_ccd`
--

INSERT INTO `options_ccd` (`id`, `nom`, `description`, `quantite`) VALUES
(1, 'GPS', 'Pour bien vous guider sur les routes.', 5),
(3, 'main d''oeuvre', 'Un super mécanicien prêt à vous aider en toutes circonstances !', 8),
(4, 'clé de 10', 'Peut être utile pour faire des réglages sur votre véhicule', 20),
(5, 'siège enfant', 'Siège prévu pour enfants de moins de 5 ans. Acheté en juillet 2015', 3),
(6, 'porte bagages', 'Coffre de toit compatible sur tous véhicules de marque Peugeot', 4),
(7, 'manomètre', 'Idéal pour tester la pression de vos pneus !', 2),
(8, 'casque', 'Indispensable pour une bonne protection sur la route', 3),
(9, 'gilet de sécurité', 'Gilet jaune de sécurité', 15),
(10, 'triangle de signalisation', 'Equipement obligatoire en cas d''accident (ce qu''on ne vous souhaite pas mais il vaut mieux prévenir que guérir)', 8),
(11, 'porte vélo', 'Très utile pour préparer une balade en forêt', 2),
(12, 'loupe', 'Pour les personnes aux yeux fatigués', 5),
(13, 'tournevis', 'Indispensable pour régler les problèmes mécaniques', 20),
(14, 'lampe', 'Lampe frontale prévue pour les travaux sous un véhicule', 4),
(15, 'gant', 'Pour les utilisateurs qui ne souhaitent pas trop se salir les mains', 5),
(16, 'attache remorque', 'Adaptable pour tous types de véhicules', 4),
(17, 'cric', 'Utilisable uniquement sur les véhicules de 2 Tonnes maximum. Permet de les élever de 40 cm.', 10);

-- --------------------------------------------------------

--
-- Structure de la table `options_item_ccd`
--

DROP TABLE IF EXISTS `options_item_ccd`;
CREATE TABLE IF NOT EXISTS `options_item_ccd` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `id_option` int(11) NOT NULL,
  `id_item` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=118 ;

--
-- Contenu de la table `options_item_ccd`
--

INSERT INTO `options_item_ccd` (`id`, `id_option`, `id_item`) VALUES
(1, 1, 8),
(2, 1, 8),
(3, 1, 13),
(4, 1, 19),
(5, 3, 3),
(6, 3, 5),
(7, 3, 6),
(8, 3, 11),
(9, 3, 1),
(10, 4, 1),
(11, 4, 2),
(12, 4, 3),
(13, 4, 4),
(14, 4, 1),
(15, 4, 5),
(16, 4, 6),
(17, 4, 7),
(18, 4, 11),
(19, 4, 12),
(20, 5, 9),
(21, 5, 10),
(22, 5, 13),
(23, 5, 17),
(24, 5, 19),
(25, 5, 20),
(26, 5, 21),
(27, 6, 9),
(28, 6, 10),
(29, 6, 14),
(30, 6, 17),
(31, 6, 20),
(32, 7, 8),
(33, 7, 9),
(34, 7, 10),
(35, 7, 13),
(36, 7, 14),
(37, 7, 15),
(38, 7, 16),
(39, 7, 17),
(40, 7, 18),
(41, 7, 19),
(42, 7, 20),
(43, 7, 21),
(44, 7, 22),
(45, 8, 21),
(46, 8, 22),
(47, 9, 8),
(48, 9, 9),
(49, 9, 10),
(50, 9, 13),
(51, 9, 14),
(52, 9, 15),
(53, 9, 16),
(54, 9, 17),
(55, 9, 18),
(56, 9, 19),
(57, 9, 20),
(58, 9, 21),
(59, 9, 22),
(60, 10, 8),
(61, 10, 9),
(62, 10, 10),
(63, 10, 13),
(64, 10, 14),
(65, 10, 15),
(66, 10, 16),
(67, 10, 17),
(68, 10, 18),
(69, 10, 19),
(70, 10, 21),
(71, 10, 22),
(72, 11, 13),
(73, 11, 14),
(74, 11, 16),
(75, 11, 17),
(76, 11, 20),
(77, 12, 1),
(78, 12, 2),
(79, 12, 3),
(80, 12, 4),
(81, 12, 5),
(82, 12, 6),
(83, 12, 11),
(84, 12, 12),
(85, 13, 1),
(86, 13, 3),
(87, 13, 4),
(88, 13, 6),
(89, 13, 7),
(90, 13, 11),
(91, 13, 12),
(92, 14, 1),
(93, 15, 1),
(94, 15, 2),
(95, 14, 3),
(96, 15, 3),
(97, 14, 4),
(98, 15, 4),
(99, 14, 5),
(100, 15, 5),
(101, 14, 6),
(102, 14, 11),
(103, 14, 7),
(104, 15, 11),
(105, 14, 12),
(106, 17, 1),
(107, 17, 3),
(108, 17, 4),
(109, 17, 6),
(110, 17, 7),
(111, 16, 8),
(112, 16, 9),
(113, 16, 10),
(114, 17, 11),
(115, 16, 14),
(116, 16, 17),
(117, 16, 20);

-- --------------------------------------------------------

--
-- Structure de la table `options_reservation_ccd`
--

DROP TABLE IF EXISTS `options_reservation_ccd`;
CREATE TABLE IF NOT EXISTS `options_reservation_ccd` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `id_reservation` int(5) NOT NULL,
  `id_option` int(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Contenu de la table `options_reservation_ccd`
--

INSERT INTO `options_reservation_ccd` (`id`, `id_reservation`, `id_option`) VALUES
(1, 1, 4),
(2, 1, 15),
(3, 2, 9),
(4, 2, 10),
(5, 2, 16),
(6, 3, 7),
(7, 5, 1),
(8, 5, 8),
(9, 6, 16),
(10, 7, 8);

-- --------------------------------------------------------

--
-- Structure de la table `reservation_ccd`
--

DROP TABLE IF EXISTS `reservation_ccd`;
CREATE TABLE IF NOT EXISTS `reservation_ccd` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `etat` varchar(50) NOT NULL DEFAULT 'réservé',
  `date_reservation` int(5) NOT NULL,
  `heure_debut` int(11) NOT NULL,
  `heure_fin` int(11) NOT NULL,
  `date_creation` date NOT NULL,
  `date_modif` date NOT NULL,
  `montant` int(5) NOT NULL DEFAULT '0',
  `note` int(5) DEFAULT NULL,
  `id_user` int(5) NOT NULL,
  `id_item` int(5) NOT NULL,
  `id_cagnotte` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Contenu de la table `reservation_ccd`
--

INSERT INTO `reservation_ccd` (`id`, `etat`, `date_reservation`, `heure_debut`, `heure_fin`, `date_creation`, `date_modif`, `montant`, `note`, `id_user`, `id_item`, `id_cagnotte`) VALUES
(1, 'payé', 1, 8, 10, '2017-12-28', '2018-01-18', 5, 5, 10, 2, NULL),
(2, 'payé', 4, 10, 18, '2017-07-07', '2017-10-09', 20, 3, 8, 17, NULL),
(3, 'réservé', 3, 8, 10, '2018-01-22', '0000-00-00', 0, NULL, 10, 15, 1),
(4, 'annulé', 4, 10, 16, '2017-11-14', '2018-01-16', 0, NULL, 5, 8, NULL),
(5, 'réservé', 2, 14, 18, '2018-01-18', '0000-00-00', 0, NULL, 3, 19, NULL),
(6, 'confirmé', 1, 16, 18, '2018-01-25', '0000-00-00', 0, NULL, 2, 10, 2),
(7, 'réservé', 2, 10, 18, '0000-00-00', '0000-00-00', 0, NULL, 7, 22, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `user_ccd`
--

DROP TABLE IF EXISTS `user_ccd`;
CREATE TABLE IF NOT EXISTS `user_ccd` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(30) CHARACTER SET utf8 NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(300) NOT NULL,
  `photo` varchar(200) DEFAULT NULL,
  `level` int(5) NOT NULL DEFAULT '0',
  `token` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Contenu de la table `user_ccd`
--

INSERT INTO `user_ccd` (`id`, `nom`, `email`, `password`, `photo`, `level`, `token`) VALUES
(1, 'Cassandre', 'Cassandre@gmail.com', '', '1.jpg', 0, '81d84765736b55d1'),
(2, 'Achille', 'Achille@gmail.com', '', '2.jpg', 0, 'e5ff8a84223e72dd'),
(3, 'Calypso', 'Calypso@gmail.com', '', '3.jpg', 1, '3e7c958251085172'),
(4, 'Bacchus', 'Bacchus@gmail.com', '', '4.jpg', 0, '70b16875e557bc8a'),
(5, 'Diane', 'Diane@gmail.com', '', '5.jpg', 0, '82755a9d80501510'),
(6, 'Clark', 'Clark@gmail.com', '', '6.jpg', 0, '1a8b49606c284987'),
(7, 'Helene', 'Helene@gmail.com', '', '7.jpg', 1, '16b57905d0aa5ee1'),
(8, 'Jason', 'Jason@gmail.com', '', '8.jpg', 1, '8b3d6fdb5b56d466'),
(9, 'Bruce', 'Bruce@gmail.com', '', '9.jpg', 0, '7182a480e9d80058'),
(10, 'Pénélope', 'Pénélope@gmail.com', '', '10.jpg', 0, '3b445b6cd3546854'),
(11, 'Ariane', 'Ariane@gmail.com', '', '11.jpg', 0, '4d3f9dc398a47805'),
(12, 'Lois', 'Lois@gmail.com', '', '12.jpg', 1, 'e5950708502f3fc8');

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `item_ccd`
--
ALTER TABLE `item_ccd`
  ADD CONSTRAINT `item_ibfk_1` FOREIGN KEY (`id_categ`) REFERENCES `categorie_ccd` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
