<?php

namespace site\views;

use site\views\VuePageHTMLBootStrap;
use site\models\Item;
use site\models\Reservation;
use site\models\User;
use Slim\Slim;

class VuePlanning{

  protected $affichage ;

  public function __construct($array) {
        $this->affichage = $array ;
    }

  /*
  * Fonction qui affiche le planning de reservation d'un item
  * return le html contentnant le planning
  */
  public function afficherPlanning(){
      $app = Slim::getInstance() ;
    $item = $this->affichage;
    $reservations = $item->reservations();
    $nom = $item->nom;
    $descr = $item['description'];

    $reservations = $item->reservations();

    $arrayReservation = array() ;
    for($i=1 ; $i<=5 ; $i++) {
        for($h=8 ; $h<=16 ; $h+=2) {
            $a = $h+2 ;
            if($h==12)
                $arrayReservation[$i]["$h-$a"] = "" ;
            else
                $arrayReservation[$i]["$h-$a"] = "Libre" ;
        }
    }

    foreach ($reservations as $r) {
        $jour = $r->date_reservation ;
        $hDeb = $r->heure_debut ;
        $hFin = $r->heure_fin ;
        $creneauDeb = $hDeb ;
        $creneauFin = $hDeb+2 ;
        while($creneauFin<=$hFin) {
            if($creneauDeb!=12 && $r->etat!="annulé") {
                $id = $r->id ;
                $arrayReservation[$jour]["$creneauDeb-$creneauFin"] = "<a href='{$app->urlFor('planningDetail', array('id'=>$id))}'>" . ucfirst($r->etat)."</a>" ;
            }
            $creneauFin+= 2 ;
            $creneauDeb+=2 ;
        }
    }

    $content = <<<HTML
    <section class="container-fluid">
    <div class="container">
    <div class="row">
      <h1 class="titre_avant_separator">$item->nom</h1>
      <hr class="separator">
    <p><u>Description :</u> $descr</p>
    <div class="table-bordered">
        <table class="table planning">
            <tr>
              <th></th>
              <th>8h-10h</th>
              <th>10h-12h</th>
              <th>12h-14h</th>
              <th>14h-16h</th>
              <th>16h-18h</th>
            </tr>
HTML;

    $arrayJour = array(1=>"Lundi", 2=>"Mardi", 3=>"Mercredi", 4=>"Jeudi", 5=>"Vendredi") ;

    for($i=1 ; $i<=5 ; $i++) {
        $content.= "<tr><td>{$arrayJour[$i]}</td>" ;
        for($h=8 ; $h<=16 ; $h+=2) {
            $a = $h+2 ;
            $valeur = $arrayReservation[$i]["$h-$a"] ;
            if($valeur=='') {
                $class = 'reservation-midi' ;
            }
            elseif (strstr($valeur, "Payé")!=null) {
                $class = 'planning-paye' ;
            }
            elseif(strstr($valeur, "Confirmé")!=null) {
                $class = 'reservation-confirme' ;
            }
            elseif(strstr($valeur, "Réservé")!=null) {
                $class = "success" ;
            }
            else {
                $class = 'planning-libre' ;
            }
            $content.= "<th class='{$class}'>{$valeur}</th>" ;
        }
        $content.= "</tr>" ;
    }

    $content.= <<<HTML
    </tr>
    </table>
    </div>
    </div>

    </div>
    </section>
HTML;

      $content.= $this->afficherFormulaireReservationItem() ;
      return $content;
  }

    public function afficherFormulaireReservationItem(){
        $app=\Slim\Slim::getInstance();
        $i=$this->affichage;
        $options = $i->options();
        $content=<<<HTML
<div class="w3-row-padding w3-padding-64 w3-container">
      <div class="w3-content">
        <h1 class="w3-padding-32 text-center">Réserver</h1>
        <form action={$app->urlFor('validerReservation')} method="post">
            <div class="form-group row">

                <div class="col-md-4 col-lg-4 col-xs-4 col-sm-4">
                    <div class="row">
                        <div class="col-md-4 col-lg-4 col-xs-4 col-sm-4 text-right">
                            <label for="jour">Jour :</label>
                         </div>
                         <div class="col-md-8 col-lg-8 col-xs-8 col-sm-8">
                            <select name="jour">
                                <option value="1" selected>Lundi</option>
                                <option value="2">Mardi</option>
                                <option value="3">Mercredi</option>
                                <option value="4">Jeudi</option>
                                <option value="5">Vendredi</option>
                            </select>
                         </div>
                    </div>
                 </div>

                <div class="col-md-4 col-lg-4 col-xs-4 col-sm-4">
                    <div class="row">
                        <div class="col-md-4 col-lg-4 col-xs-4 col-sm-4 text-right">
                            <label for="debut">De :</label>
                         </div>
                         <div class="col-md-8 col-lg-8 col-xs-8 col-sm-8">
                            <select name="debut">
                                <option value="8" selected>8h</option>
                                <option value="10">10h</option>
                                <option value="14">14h</option>
                                <option value="16">16h</option>
                            </select>
                         </div>
                     </div>
                 </div>

                 <div class="col-md-4 col-lg-4 col-xs-4 col-sm-4">
                    <div class="row">
                        <div class="col-md-4 col-lg-4 col-xs-4 col-sm-5 text-right">
                            <label for="debut">Jusqu'à :</label>
                         </div>
                         <div class="col-md-8 col-lg-8 col-xs-8 col-sm-7">
                            <select name="fin">
                                <option value="10" selected>10h</option>
                                <option value="12">12h</option>
                                <option value="16">16h</option>
                                <option value="18">18h</option>
                            </select>
                         </div>
                     </div>
                 </div>

                 <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6">
                    <div class="row">
                        <div class="col-md-4 col-lg-4 col-xs-4 col-sm-5 text-right">
                            <label for="debut">Montant</label>
                         </div>
                         <div class="col-md-8 col-lg-8 col-xs-8 col-sm-7">
                            <input type="number" name="montant">
                         </div>
                     </div>
                 </div>

                <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6">
                    <div class="row">
                        <div class="col-md-4 col-lg-4 col-xs-4 col-sm-5 text-right">
                            <label for="debut">Cagnotte</label>
                         </div>
                         <div class="col-md-8 col-lg-8 col-xs-8 col-sm-7">
                            <select name="cagnotte">
                                <option value="0" selected>Non</option>
                                <option value="1">Oui</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6">
                    <div class="row">
                        <div class="col-md-4 col-lg-4 col-xs-4 col-sm-5 text-right">
                            <label for="debut">Options</label>
                         </div>
                         <div class="col-md-8 col-lg-8 col-xs-8 col-sm-7">
HTML;
        foreach ($options as $o) {
          $nom=$o->nom;
          $content= $content . <<<HTML
                              <input type="checkbox" name="$nom" value="$nom" />$nom</br>

HTML;
        }


        $content= $content . <<<HTML
                        </div>
                    </div>
                </div>

                <input type="hidden" name="id_item" value=$i->id>
            </div>

            <div class="col-md-12 text-center" style="margin-top:20px">
                <button type="submit" name="Valider" values="Valider" class="btn">Valider</button>
            </div>
        </form>
    </div>
</div>

HTML;
        return $content;
    }


  public function detailPlanning()
  {
      $res = $this->affichage;
      $item = $res->item();
      $nomI = $item['nom'];
      $descr = $item['description'];

      $etat = $res->etat;
      $date_res = $res->date_reservation;
      $heure_debut = $res->heure_debut;
      $heure_fin = $res->heure_fin;
      $date_creation = $res->date_creation;
      $date_modif = $res->date_modif;
      $montant = $res->montant;
      $note = $res->note;
      $id_user = $res->id_user;

      $user = User::where('id', '=', $id_user)->first();
      $nom = $user->nom;

      $jour = "";
      switch ($date_res) {
          case 1:
              $jour = "lundi";
              break;
          case 2:
              $jour = "mardi";
              break;
          case 3:
              $jour = "mercredi";
              break;
          case 4:
              $jour = "jeudi";
              break;
          case 5:
              $jour = "vendredi";
              break;
      }

      $content = <<<HTML
        <section class="container-fluid">
        <div class="container">
        <div class="row">
          <h1 class="titre_avant_separator">$nomI</h1>
    			<hr class="separator">
        <p>Description : $descr</p>
        <p>etat : $etat</p>
        <p>reservé par : $nom</p>
        <p>jour : $jour</p>
        <p>heure de debut : $heure_debut h</p>
        <p>heure de fin : $heure_fin h</p>
        <p>date de creation : $date_creation</p>
        <p>date de modification : $date_modif</p>
        <p>montant : $montant</p>
        <p>note : $note</p>
        </div>
        </div>
        </section>
HTML;

        return $content;
}



  public function render($i){
    switch($i){
      case 1 : $html = $this->afficherPlanning();
        break;
      case 2 : $html = $this->detailPlanning();
        break;
    }
    $v = new VuePageHTMLBootStrap($html);
    echo $v->showHTML();
	if(isset($_SESSION['periodeReservationError']))var_dump($_SESSION['periodeReservationError']);
  }

}
