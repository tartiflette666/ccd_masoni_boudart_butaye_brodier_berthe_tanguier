<?php

namespace site\views;

use Slim\Slim;

class VueCompte{

  protected $app,$content,$erreurCo, $erreurInscr,$erreurModif;
    const CONNEXION = 1 ;
    const INSCRIPTION = 2 ;
    const COMPTE = 3 ;
    const INSCRIPTION_CONNEXION = 4 ;
    const MODIF = 5 ;

function __construct(){
  $this->app = Slim::getInstance();
  if(isset($_SESSION['erreurConnexion']))
      $this->erreurCo = $_SESSION['erreurConnexion'];
  if(isset($_SESSION['erreurInscription']))
        $this->erreurInscr=$_SESSION['erreurInscription'];
  if(isset($_SESSION['erreur_modif']))
        $this->erreurModif=$_SESSION['erreur_modif'];

}


  public function formulaire_connexion(){



    unset($_SESSION['erreurConnexion']);
    $this->content .=  <<<HTML

    <div class="w3-row-padding w3-padding-64 w3-container">
      <div class="w3-content">
                <h1 class="erreur"> $this->erreurCo </h1>
                    <h1 class="w3-padding-32">Connexion</h1>
                    <form id="form1" method="POST" action="{$this->app->urlFor('valider_connexion')}">
                      <label for="form1_id">Identifiant : </label>
                      </br>
                      <input required type="text" id="form1_id" name="nom" value="">
                      </br>
                      </br>
                      <label for="form1_id2">Mot de passe : </label>
                      </br>
                      <input required  type="password" id="form1_id2" name="password" value="">
                      </br>
                      </br>
                      <label style="color:#cc0000" for="form2_id7"></label>
                      <button required class="w3-button w3-black w3-padding-large w3-large w3-margin-top" type="submit" name="valider_co" value="valid_f1">Valider</button>
                    </form>
                </div>
              </div>
HTML;
return $this->content;
  }

  public function formulaire_inscription(){

  unset($_SESSION['erreurInscription']);
  $this->content =  <<<HTML
  <div class="w3-row-padding w3-padding-64 w3-container">
      <div class="w3-content">
              <h1 class="erreur"> $this->erreurInscr </h1>
                  <h1 class="w3-padding-32">Inscription</h1>
                  <form id="form1" method="POST" action="{$this->app->urlFor('valider_inscription')}">
                    <label for="form1_id">Identifiant : </label>
                    </br>
                    <input required type="text" id="form1_id" name="nom" value="">
                    </br>
                    </br>
                    <label for="form1_id2">Mot de passe : </label>
                    </br>
                    <input required  type="password" id="form1_id2" name="password" value="">
                    </br>
                    </br>
                    <label for="form2_id3">Confirmer mot de passe : </label>
                    </br>
                    <input required type="password" id="form1_id3" name="confirmedpassword" value="">
                    </br>
                    </br>
                    <label for="form2_id4">Adresse email : </label>
                    </br>
                    <input required type="email" id="form1_id4" name="email" value="">
                    </br>
                    </br>
                    <label style="color:#cc0000" for="form2_id7"></label>
                    </br>
                    <button required class="w3-button w3-black w3-padding-large w3-large w3-margin-top" type="submit" name="valider_inscr" value="valid_f1">Valider</button>
                  </form>
              </div>
            </div>
HTML;
return $this->content;

  }

  public function formulaires() {
      $content = <<<HTML
<section class="container-fluid">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12 text-center">
HTML;
      $content.=$this->formulaire_connexion() ;
      $content.= <<<HTML
        </div>
        <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12 text-center">
HTML;
      $content.= $this->formulaire_inscription()."</div></div></div></section>" ;
      return $content ;


  }


  public function afficher_compte(){
    if (isset($_SESSION['user'])){
        $user = $_SESSION['user'];
        $nom = $user->nom;
        $email = $user->email;
        $rang = $user->level;

        if ($rang == 0){
          $rang="Vous n'êtes pas un administrateur." ;
          }
        }
        $this->content =
        <<<HTML
        <div class="w3-row-padding w3-padding-64 w3-container">
          <div class="w3-content">
            <div class="w3-twothird">
              <h1>Informations sur le compte :</h1>
              <h5 class="w3-padding-16">Identifiant : $nom</h5>
              <h5 class="w3-padding-16">Email : $email</h5>
              <h5 class="w3-padding-16"> $rang </h5>
            </br>
            <a href="{$this->app->urlFor('verif_modif_compte')}" class="w3-button w3-black w3-padding-large w3-large">Modifier mon compte </a>
            </div>
          </div>
        </div>
HTML;

return $this->content;
    }


  public function formulaire_modif_compte(){
unset($_SESSION['erreur_modif']);
if (isset($_SESSION['user'])){
    $user = $_SESSION['user'];}

  $this->content = <<<HTML
      <div class="w3-row-padding w3-padding-64 w3-container">
      <div class="w3-content">
        <h1 class="erreur"> $this->erreurModif </h1>
        <div class="w3-twothird">
            <h1 class="w3-padding-32">Modification du compte</h1>
            <form id="modifier" method="POST" action="{$this->app->urlFor('valider_modif_compte')}">
              <label for="1">Ancien mot de passe : </label>
              </br>
              <input required type="password" id="1" name="old_password" value="">
              </br>
              </br>
              <label for="2">Nouveau mot de passe : </label>
              </br>
              <input required type="password" id="2" name="password" value="">
              </br>
              </br>
              <label for="3">Confirmer mot de passe : </label>
              </br>
              <input required type="password" id="3" name="confirmedpassword" value="">
              </br>
              </br>
              <label for="form2_id4">Adresse email : </label>
              </br>
              <input required type="email" id="form1_id4" name="email" value="$user->email">
              </br>
            </br>
            <button required class="w3-button w3-black w3-padding-large w3-large w3-margin-top" type="submit" name="valider_modif" value="valid_f1">Valider</button>
            </form>
          </div>
        </div>
      </div>
HTML;

return $this->content;
    }


    public function render($i){
      switch($i){
        case self::CONNEXION : $html = $this->formulaire_connexion();
          break;
        case self::INSCRIPTION: $html = $this->formulaire_inscription();
          break;
        case self::COMPTE: $html = $this->afficher_compte();
          break;
        case self::INSCRIPTION_CONNEXION : $html = $this->formulaires() ;
          break ;
        case self::MODIF : $html = $this->formulaire_modif_compte() ;
          break;
      }
      $v = new VuePageHTMLBootStrap($html);
      echo $v->showHTML();
    }

}
