<?php

namespace site\views;

use site\models\Categorie;
use site\models\Item;
use Slim\Slim;
use site\models\User;

class VueCatalogue
{

    const NOMBRE_ITEM_PAR_PAGE = 3;
    protected $affichage;


    /**
     * Affichage principale de la vue de la liste des voitures
     */
    public function afficherLeCatalogueDesVoitures($page)
    {
        $content = $this->debutConteneurPagePrincipal();
        $content .= $this->afficherEntete("Catalogue Des Voitures");
        $content .= $this->formulaireDeParcour($page, 1);
        $content .= $this->afficherEnsembleDesVoitures($page);
        $content .= $this->finConteneurPagePrincipal();

        $v = new VuePageHTMLBootStrap($content);
        echo $v->showHTML();
    }

    /**
     *
     * @return string
     */
    public function debutConteneurPagePrincipal()
    {
        return <<<HTML
    <section class="container-fluid theme_page_recherche_theme">
    <div class="container">
        <div class="row">
HTML;
    }

    /**
     * Affiche l'entete de la liste
     * (Le titre de la liste)
     * @param String
     */
    public function afficherEntete($nomEntete)
    {
        return <<<HTML
            <h2 class="titre_avant_separator">$nomEntete</h2>
            <hr class="separator">  
HTML;
    }

    /**
     * Formulaire des boutons precedents et suivant
     * @return string
     *
     */
    public function formulaireDeParcour($page, $idcateg)
    {
        $app = Slim::getInstance();
        $nbitems = Item::retournerNombreDitem($idcateg);
        $nb = round($nbitems / self::NOMBRE_ITEM_PAR_PAGE) - 1;
        if ($page > 0) $precedent = $page - 1;
        else $precedent = 0;
        if ($page < $nb)
            $suivant = $page + 1;
        else $suivant = $page;

        $nomCategorie = Categorie::retournerNomDeLaCategorie($idcateg);
        return <<<HTML
    <nav aria-label="navigation">
      <ul class="pager">
        <li class="previous"><a href={$app->urlFor(strtolower($nomCategorie), array('page' => $precedent))} title="Précédent">Précédent</a></li>
        <li class="next"><a href={$app->urlFor(strtolower($nomCategorie), array('page' => $suivant))}>Suivant</a></li>
      </ul>
    </nav>
HTML;
    }


    /**
     * Affiche l'ensemble des voitures
     * @return String contenu du HTML
     */
    public function afficherEnsembleDesVoitures($page)
    {
        //id de la catégorie pour les voitures : 1
        //$liste_item=Item::ensembleDesItemsDuneCategorie(1);

        $min_id = $page * self::NOMBRE_ITEM_PAR_PAGE + 1;
        $max_id = $min_id + 2;

        $nbitem = Item::retournerNombreDitem(1);

        if ($max_id <= $nbitem) {
            $liste_item = Item::select('*')->where('id_categ', '=', 1)
                ->skip($min_id)->take(self::NOMBRE_ITEM_PAR_PAGE)->get();
        } else {
            $liste_item = null;
        }
        $content = "";
        if ($liste_item != null) {
            $nombreItemParPage = self::NOMBRE_ITEM_PAR_PAGE;

            foreach ($liste_item as $voiture) {
                if ($nombreItemParPage == 3) {
                    $content .= <<<HTML
    <div class="row">
       <div class="container-fluid col-md-12">
           <div class="col-md-2"></div>
              <div class="col-md-8">
HTML;
                }
                $content .= $this->afficherUnItem($voiture);
                if ($nombreItemParPage <= 1) {

                    $content .= <<<HTML
            </div>
            </div>
            </div>
HTML;

                    $nombreItemParPage = 3;
                }
                $nombreItemParPage--;
            }
            $content .= <<<HTML
        </div>
            </section>
        </div>
HTML;

            return $content;
        }
    }


    /**
     * @return string contenu HTML
     */
    public function afficherUneVoiture(Item $voiture)
    {
        return <<<HTML
    <div class="container item_theme">
       <article class="description_items_catalogue">
                <h3>$voiture->nom</h3>
               <p>$voiture->description</p>
       </article>
    </div>
HTML;
    }

    /**
     * Retourne la fin du conteneur principal
     * @return string
     */
    public function finConteneurPagePrincipal()
    {
        return <<<HTML
        </div>
</div>
    </section>
HTML;
    }

    /**
     * Affichage principale de la vue de la liste des ateliers
     */
    public function afficherLeCatalogueDesAteliers($page)
    {
        $content = $this->debutConteneurPagePrincipal();
        $content .= $this->afficherEntete("Catalogue Des Ateliers");
        $content .= $this->formulaireDeParcour($page, 2);
        $content .= $this->afficherEnsembleDesAteliers($page);
        $content .= $this->finConteneurPagePrincipal();

        $v = new VuePageHTMLBootStrap($content);
        echo $v->showHTML();

    }

    /**
     * Affiche la vue de l'ensemble des ateliers
     */
    public function afficherEnsembleDesAteliers($page)
    {
        //id de la catégorie pour les voitures : 1
        //$liste_item=Item::ensembleDesItemsDuneCategorie(1);

        $min_id = $page * self::NOMBRE_ITEM_PAR_PAGE + 1;
        $max_id = $min_id + 2;

        $nbitem = Item::retournerNombreDitem(2);

        if ($max_id <= $nbitem) {
            $liste_item = Item::select('*')->where('id_categ', '=', 2)
                ->skip($min_id)->take(self::NOMBRE_ITEM_PAR_PAGE)->get();
        } else {
            $liste_item = null;
        }
        $content = "";
        if ($liste_item != null) {
            $nombreItemParPage = self::NOMBRE_ITEM_PAR_PAGE;

            foreach ($liste_item as $atelier) {
                if ($nombreItemParPage == 3) {
                    $content .= <<<HTML
    <div class="row">
       <div class="container-fluid col-md-12">
           <div class="col-md-3"></div>
              <div class="col-md-6">
HTML;
                }
                $content .= $this->afficherUnItem($atelier);
                if ($nombreItemParPage <= 1) {

                    $content .= <<<HTML
            </div>
            </div>
            </div>
HTML;

                    $nombreItemParPage = 3;
                }
                $nombreItemParPage--;
            }
            $content .= <<<HTML
        </div>
            </section>
        </div>
HTML;

            return $content;
        }
    }

    /**
     * @return string contenu HTML
     */
    public function afficherUnItem(Item $item)
    {
        $app = Slim::getInstance();
        $racine = substr($_SERVER['SCRIPT_NAME'], 0, strlen($_SERVER['SCRIPT_NAME']) - 9);
        $token = $item->token;
        $content="";
        $content.= <<<HTML
        <a href={$app->urlFor('item', array('token' => $token))}>
    <div class="container item_theme">
       <div class="row">
            <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6 item-theme-div">
               <img src="{$racine}/img/item/{$item->image}" name=$item->nom />
            </div>
            <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6">
               <h3>$item->nom</h3>
               <p>$item->description</p>
HTML;
        if(isset($_SESSION['user'])){
            if(User::retournerLevelUser($_SESSION['user']->nom) >=1) {
                $content .= <<<HTML
<a href={$app->urlFor('modification_item',array('token' => $item->token))} style="color:red">modifier</a>
HTML;
            }
        }
        $content.=<<<HTML
            </div>
       </div>
    </div>
</a>
HTML;
    return $content;
    }
}
