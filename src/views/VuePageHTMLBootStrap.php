<?php

namespace site\views;
use Slim\Slim;
use site\models\User;

class VuePageHTMLBootStrap {

    private $content ;
    private $app;
    public function __construct($c) {
        $this->content = $c ;
        $this->app= Slim::getInstance() ;
    }

    private function header() {

        $racine = substr($_SERVER['SCRIPT_NAME'],0,strlen($_SERVER['SCRIPT_NAME'])-9) ;
        return <<<HTML
        <html>
            <head>
                <meta charset="utf-8">
                <title>Garages Hand2Hand</title>

                <!--- font -->
                <link href="https://fonts.googleapis.com/css?family=Anton" rel="stylesheet">

                <!-- css -->
                <link rel="stylesheet" href="{$racine}css/bootstrap.min.css">
                <link rel="stylesheet" href="{$racine}css/style.css">
                <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
            </head>
            <body>
HTML;
    }

    /**
     * menu de navigation
     * @return string
     */
    private function navbar()
    {
        $res = <<<HTML
        <!-- header -->
        <!--<header class="container-fluid header">
            <div class="container">
                <!--<a href={$this->app->urlFor('root')} class="logo">mon site</a>-->
                <!--<nav class="menu">
                    <!--<a href={$this->app->urlFor('home')}> Accueil </a>-->
                <!--</nav>
            </div>
        </header>-->
        <!-- end header -->
<nav class="container-fluid header">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href={$this->app->urlFor('root')} class="logo">Garages Hand-2-Hand</a>
        </div>
    <div id="navbar" class="collapse navbar-collapse">
        <ul class="nav navbar-nav">
            <li class="active"><a href="{$this->app->urlFor('home')}">Accueil</a></li>
HTML;
        if (isset($_SESSION['user'])) {
$res .= <<<HTML
            <li class=\"deconnexion\"><a href="{$this->app->urlFor('deconnexion')}">Se déconnecter</a></li>
            <li class=\"réservations\"><a href={$this->app->urlFor('utilisateur',array('id'=>$_SESSION['user']->id))}>Mes réservations</a></li>
            <li class=\"compte\"><a href="{$this->app->urlFor('voir_compte')}">Mon compte</a></li>
            <li class=\"compte\"><a href="{$this->app->urlFor('utilisateurs')}">Liste utilisateurs</a></li>
HTML;
            if($_SESSION['user']->nom != null) {
                if ((User::retournerLevelUser($_SESSION['user']->nom)) >= 1) {
                    $res .= <<<HTML
            <li class="administration"><a href="{$this->app->urlFor('administration')}">Administration</a></li>
HTML;
                }
            }
        } else {
$res .= <<<HTML
            <li class=\"login\"><a href="{$this->app->urlFor('verif_inscription_connexion')}">Authentification</a></li>
HTML;
        }


        $res .= <<<HTML
          </ul>
        </div>
      </div>
    </nav>
HTML;
        return $res;
    }

    /**
     * footer
     * @return string
     */
    private function footerSimple() {
        return <<<HTML
        <!-- footer / contact -->
        <footer class="container-fluid footer">
            <div class="container">
                <p>Créé par la Team Tartiflette</p>
            </div>
        </footer>
HTML;
    }

    /**
     * footer
     * @return string
     */
    private function footerComplet() {
        return <<<HTML
         <!-- footer / contact -->
    <footer class="container-fluid footer">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                        <form class="form-horizontal" action="" method="post">
                            <legend class="text-center">Contactez-nous</legend>

                            <!-- Name input-->
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="name">Nom</label>
                                <div class="col-md-9">
                                    <input id="name" name="name" type="text" placeholder="Votre nom" class="form-control">
                                </div>
                            </div>

                            <!-- Email input-->
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="email">E-mail</label>
                                <div class="col-md-9">
                                    <input id="email" name="email" type="text" placeholder="Votre email" class="form-control">
                                </div>
                            </div>

                            <!-- Message body -->
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="message">Message</label>
                                <div class="col-md-9">
                                    <textarea class="form-control" id="message" name="message" placeholder="Veuillez saisir votre message here..." rows="5"></textarea>
                                </div>
                            </div>

                            <!-- Form actions -->
                            <div class="form-group">
                                <div class="col-md-12 text-center">
                                    <button type="submit" class="btn btn-primary btn-lg home-submit">Envoyer</button>
                                </div>
                            </div>
                        </form>

                        <p>Créé par la Team Tartiflette</p>
                </div>

            </div>
        </div>
    </footer>
HTML;
    }

    /**
     * Appel le fichier bootstrap avec le JS
     * @return string
     */
    public function appelJs(){
        $racine = substr($_SERVER['SCRIPT_NAME'],0,strlen($_SERVER['SCRIPT_NAME'])-9) ;
        return <<<HTML
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js">
            </script>
            <script src="js/bootstrap.min.js"></script>
HTML;
    }

    /**
     * @return stringAffichage
     */
    public function showHTML() {
        echo $this->header().$this->navbar().$this->content.$this->footerSimple().$this->appelJs()."</html>" ;
    }

    /**
     * Affichage sans footer
     */
    public function showHTMLAvecFooter(){
        echo $this->header().$this->navbar().$this->content.$this->appelJs().$this->footerComplet()."</html>";
    }
}
