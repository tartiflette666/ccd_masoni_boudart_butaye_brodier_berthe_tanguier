<?php

namespace site\views;

use site\models\Categorie;
use Slim\Slim;

class VuePageHome
{

    private $app;

    public function home(){
        $this->app = Slim::getInstance();
        // Il faudra ajouter les liens des categories une fois que la route sera creee
        $content=<<<HTML
        <!-- bannière -->
            <section class="container-fluid banner">
                <img src="img/banniere.jpeg" alt="bannière du site"/>
            </section>
        <!-- end bannière -->
        <section class="container-fluid">
                <div class="container">
                    <div class="row">
                        <h2 class="titre_avant_separator">Bienvenue sur le site de Garages Hand-2-Hand !</h2>
                        <hr class="separator">

                        <article class="col-md-2 col-lg-2 col-xs-12 col-sm-12"></article>
                        <article class="col-md-8 col-lg-8 col-xs-12 col-sm-12">
                            <p class="text-center">
                                Nous sommes une association loi 1901 qui propose la location et la réparation de véhicules. Vous avez besoin d'aide dans une ambiance fun et détendue ? Parcourez notre site ou contactez nous pour accéder aux différents services que nous proposons !
                            </p>
                        </article>
                    </div>
                </div>
            </section>

    <section class="container-fluid">
                <div class="container">
                    <div class="row">
                        <h2 class="titre_avant_separator">Catégories</h2>
                        <hr class="separator">
HTML;
        $categories = Categorie::get()->toArray();
        foreach ($categories as $categorie) {
            $content .= <<<HTML
            <a href={$this->app->urlFor(strtolower($categorie['nom']),array('page' => 0))}>
                        <article class="col-md-6 col-lg-6 col-xs-12 col-sm-12" style="height:250px">
                            <div class="media">
                                <div class="media-left">

                                    <img src="img/categorie/{$categorie['image']}" alt="Image de catégorie" class="media-object" style="width:150px">

                                </div>
                                <div class="media-body">
                                    <h1 class="media-heading">{$categorie['nom']}</h1>
                                    <p>{$categorie['description']}</p>
                                </div>
                            </div>
                        </article>
</a>
HTML;
        }
        $content .= <<<HTML
                    </div>
                </div>
            </section>
HTML;
        echo (new VuePageHTMLBootStrap($content))->showHTMLAvecFooter();
    }
}
