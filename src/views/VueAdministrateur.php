<?php
/**
 * Created by PhpStorm.
 * User: Jinsei
 * Date: 08/02/2018
 * Time: 17:10
 */

namespace site\views;

use site\models\User;
use site\models\Reservation;
use Slim\Slim;

class VueAdministrateur
{
    /**
     * Affichage du panneau de controle de l'administrateur
     */
    public function affichagePanneauDeControl(){
        $content="";
        $content.=$this->affichageEntete("Panneau de gestion");
        $content.=$this->affichageDesCategoriesDeControl();
        $content.=$this->finPage();

        $v=new VuePageHTMLBootStrap($content);
        echo $v->showHTML();
    }

    /**
     * Affiche l'entete de la page
     * (Le titre de la liste)
     * @param String
     */
    public function affichageEntete($nomEntete){
        return <<<HTML
        <section class="container-fluid">
                <div class="container">
                    <div class="row">
            <h2 class="titre_avant_separator">$nomEntete</h2>
            <hr class="separator">  
HTML;
    }

    /**
     * Gestion de ligne de boostrap
     * @return string
     */
    public function debutDeLigneBoostrap(){
        return <<<HTML
<div class="container" >
HTML;
    }

    /**
     * Fin de la ligne
     * @return string
     */
    public function finLigneBoostrap(){
        return <<<HTML
        </div>
</div>
HTML;

    }

    /**
     * Gère l'affichage des catégories disponibles pour l'administrateur
     * @return string
     */
    public function affichageDesCategoriesDeControl(){
        $content="";
        $content.=$this->debutDeLigneBoostrap();
        $content.=$this->ajouterUneCategorie("utilisateurs","Gestion des utilisateurs","Dans cette section vous pourrez gérer les informations des utilisateurs et leurs droits d'accès");
        $content.=$this->ajouterUneCategorie("admin_choix_item","Gestion des items","Gérez les items présents dans votre base de données");
        $content.=$this->finLigneBoostrap();
        $content.=$this->ajouterUneCategorie("afficher_creation_categories",'Gestion Categorie',"Ajoutez ou modifiez des categories");
        $content.=$this->ajouterUneCategorie("afficher_reservations",'Gestion des réservations',"Confirmer ou annuler des réservations");
        $content.=$this->finLigneBoostrap();

        return $content;
    }

    /**
     * Ajoute une catégorie
     * @param $url
     * @param $nomCategorie
     * @param $description
     * @return string
     */
    public function ajouterUneCategorie($url,$nomCategorie,$description){
        $app=Slim::getInstance();
        $content=<<<HTML
        <a href={$app->urlFor($url)}>
        <article class="col-md-6 col-lg-6 col-xs-12 col-sm-12"> 
            <div id="signupbox" style=" margin-top:50px" class="mainbox ">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="panel-title">{$nomCategorie}</div>
                        <div style="float:right; font-size: 85%; position: relative; top:-10px"><a id="signinlink" href="/accounts/login/">Aide</a></div>
                    </div> 
                </div>
                <div class="panel-body" >
                    <p>{$description}</p>
                </div>
        </div>
        </article>
</a>
HTML;
        return $content;
    }

    /**
     * Cloture la page html
     * @return string
     */
    public function finPage(){
        return <<<HTML
    </div>
</div>
</div>
HTML;

    }

    /**
     * Affiche la page qui permet à l'utilisateur de choisir quel catalogue il souhaite consulter
     */
    public function afficherChoixDeCategorieDitem(){
        $content="";
        $content.=$this->debutDeLigneBoostrap();
        $content.=$this->ajouterUneCategorie("atelier","Ateliers","");
        $content.=$this->ajouterUneCategorie("vehicule","voitures","");
        $content.=$this->finLigneBoostrap();
        $v=new VuePageHTMLBootStrap($content);
        echo $v->showHTML();
    }

    /**
     * Affiche les réservations de tous les utilisateurs
     */
    public function afficherReservations() {
        $app = Slim::getInstance();
        $content = $this->debutDeLigneBoostrap();
        $reservations = Reservation::where('etat', '=', 'réservé')->get();
        $content .= <<<HTML
        <h2 class="titre_avant_separator">Réservations en attente de confirmation</h2>
        <hr class="separator">
        <form action={$app->urlFor('confirmer_reservations')} method="post">
HTML;
        if(count($reservations) != 0) {
            foreach ($reservations as $reservation) {
                $user = User::select('nom')->where('id', '=', $reservation['id_user'])->get();
                $content .= "
                <div class='checkbox'>
                    <label><input type='checkbox' name='choixReserv[]' value={$reservation['id']}>Réservation n°{$reservation['id']}</label>
                    <p>De {$user[0]['nom']}, le {$reservation['date_creation']}</p>
                </div>";
            }
        }
        $content .= <<<HTML
        <button type="submit" class="btn btn-default" name="confirmer" value="confirmer_fct">Confirmer</button>
        <button type="submit" class="btn btn-default" name="annuler" value="annuler_fct">Annuler</button>
        </form>
HTML;
        $content.=$this->finLigneBoostrap();
        $v=new VuePageHTMLBootStrap($content);
        echo $v->showHTML();
    }

    /**
     * Affiche un formulaire de création de catégorie
     */
    public function afficherCreationCategories() {
        $app = Slim::getInstance();
        $content = $this->debutDeLigneBoostrap();
        $content .= <<<HTML
        <h2 class="titre_avant_separator">Créer une nouvelle catégorie</h2>
        <hr class="separator">
        <form action={$app->urlFor('creer_categorie')} method="post">
            <div class="form-group">
                <label for="nom">Nom :</label>
                <input type="text" class="form-control" id="nomCat">
            </div>
            <div class="form-group">
                <label for="description">Password:</label>
                <textarea class="form-control" rows="5" id="descriptionCat"></textarea>
            </div>
            <button type="submit" class="btn btn-default" name="ajouter_cat" value="ajouter_cat_fct">Ajouter la catégorie</button>
        </form>
HTML;
        $content.=$this->finLigneBoostrap();
        $v=new VuePageHTMLBootStrap($content);
        echo $v->showHTML();
    }
}