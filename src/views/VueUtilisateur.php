<?php

namespace site\views;

use site\views\VuePageHTMLBootStrap;
use site\models\User;
use site\models\Item;
use Slim\Slim;

class VueUtilisateur{

  protected $affichage ;

  public function __construct($array) {
        $this->affichage = $array ;
    }

  /*
  * Fonction qui affiche la liste de tous les utilisateurs
  */
  public function afficherListeUtilisateurs(){
    $this->app = Slim::getInstance();
    $liste_util = User::all();
    $racine = substr($_SERVER['SCRIPT_NAME'],0,strlen($_SERVER['SCRIPT_NAME'])-9);

    $content = <<<HTML
        <section class="container-fluid">
        <div class="container">
          <h1 class="titre_avant_separator">Liste des utilisateurs</h1>
          <hr class="separator">
        <ul>
HTML;

    foreach ($liste_util as $l) {
      $id = $l->id;
      $nom = $l->nom;
      $photo = $l->photo;
      $level = $l->level;
      $lev = "";
      $couleur = "";
      if($level==0){
        $lev = "utilisateur";
      }
      else{
        $lev = "admin";
        $couleur = "red";
      }

        if (file_exists('img/user/' . $photo) && isset($photo)) {
            $img = <<<HTML
                <img src={$racine}img/user/{$photo} name={$photo}/>
HTML;
        } else {
            $img = <<<HTML
                <img src='{$racine}img/user/img_avatar1.png' class="media-object" style="width:100px">
HTML;
        }

      $racine = substr($_SERVER['SCRIPT_NAME'], 0, strlen($_SERVER['SCRIPT_NAME']) - 9);

      $content = $content . <<<HTML
      <div class="col-md-4 col-lg-4 col-xs-4 col-sm-4" style="margin-top: 15px; margin-bottom: 30px">
          <a href={$this->app->urlFor('utilisateur',array('id' => $id))} >Nom : $nom</a>
    
          <div style="color: $couleur"><i>$lev</i></div>
          $img
      </div>
        
HTML;
    }

    $content = $content . <<<HTML
    </ul>
  </div>
  </div>
  </section>
HTML;

      return $content;
  }

  /*
  * Fonction qui affiche un utilisateur
  */
  public function afficherUtilisateur(){
    $user = $this->affichage;
    $nom = $user->nom;
    $reservations = $user->reservations();

    $content = <<<HTML
        <section class="container-fluid">
        <div class="container">
        <div class="row">
          <h1 class="titre_avant_separator">Planning de $nom</h1>
    			<hr class="separator">
          <ul>
HTML;

      $content.= $this->afficherPlanning() ;

    if(isset($_SESSION['user'])){
        $app=Slim::getInstance();
        if(User::retournerLevelUser($_SESSION['user']->nom) == 1){
            $content.=<<<HTML
    <form action={$app->urlFor('modification_droit',array('id' => $user->id))} method="post">
        <INPUT type= "radio" name="level" value=0> Level 0 (utilisateur)
        <INPUT type= "radio" name="level" value=1> Level 1 (Administrateur)
        <button type="submit" name="valider">Valider</button>
</form>
HTML;

            // radio bouton mettre admin
        }
    }
$content = $content . <<<HTML
</ul>
</div>
</div>
</section>
HTML;

  return $content;
  }

  public function afficherPlanning()
  {
      $user = $this->affichage;
      $reservations = $user->reservations();
      $content = "Cet utilisateur n'a réservé aucun item." ;
      if (isset($reservations)) {
          $arrayReservation = array() ;
          for($i=1 ; $i<=5 ; $i++) {
              for($h=8 ; $h<=16 ; $h+=2) {
                  $a = $h+2 ;
                  if($h==12) {
                      $arrayReservation[$i]["$h-$a"]['etat'] = "";
                      $arrayReservation[$i]["$h-$a"]['nom'] = "" ;
                  }
                  else {
                      $arrayReservation[$i]["$h-$a"]['nom'] = "Libre";
                      $arrayReservation[$i]["$h-$a"]['etat'] = "Libre" ;
                  }
              }
          }

          foreach ($reservations as $r) {
              $id = $r->id_item;
              $i = Item::where('id', '=', $id)->first();
              $nomI = $i->nom;
              $jour = $r->date_reservation;
              $hDeb = $r->heure_debut;
              $hFin = $r->heure_fin;
              $creneauDeb = $hDeb;
              $creneauFin = $hDeb + 2;
              while ($creneauFin <= $hFin) {
                  if ($creneauDeb != 12 && $r->etat != "annulé") {
                      $arrayReservation[$jour]["$creneauDeb-$creneauFin"]['nom'] = $nomI;
                      $arrayReservation[$jour]["$creneauDeb-$creneauFin"]['etat'] = $r->etat;
                  }
                  $creneauFin += 2;
                  $creneauDeb += 2;
              }
          }

          $content = <<<HTML
    <section class="container-fluid">
    <div class="container">
    <div class="row">
    <div class="table-bordered">
        <table class="table planning">
            <tr>
              <th></th>
              <th>8h-10h</th>
              <th>10h-12h</th>
              <th>12h-14h</th>
              <th>14h-16h</th>
              <th>16h-18h</th>
            </tr>
HTML;

          $arrayJour = array(1 => "Lundi", 2 => "Mardi", 3 => "Mercredi", 4 => "Jeudi", 5 => "Vendredi");

          for($i=1 ; $i<=5 ; $i++) {
              $content.= "<tr><td>{$arrayJour[$i]}</td>" ;
              for($h=8 ; $h<=16 ; $h+=2) {
                  $a = $h+2 ;
                  $valeur = $arrayReservation[$i]["$h-$a"]['etat'] ;
                  $nom = $arrayReservation[$i]["$h-$a"]['nom'] ;
                  if($valeur=='') {
                      $class = 'reservation-midi' ;
                  }
                  elseif (strstr($valeur, "payé")!=null) {
                      $class = 'planning-paye' ;
                  }
                  elseif(strstr($valeur, "confirmé")!=null) {
                      $class = 'reservation-confirme' ;
                  }
                  elseif(strstr($valeur, "réservé")!=null) {
                      $class = "success" ;
                  }
                  else {
                      $class = 'planning-libre' ;
                  }
                  $content.= "<th class='{$class}'>{$nom}</th>" ;
              }
              $content.= "</tr>" ;
          }

          $content.= <<<HTML
    </tr>
    </table>
    
    <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
      <div>Légende</div>
    </div>
    <div class="col-md-3 col-lg-3 col-xs-3 col-sm-3">
      <p class="planning-libre">Pas de réservation</p>
    </div>
    <div class="col-md-3 col-lg-3 col-xs-3 col-sm-3">
      <p style="background-color:lightblue">Réservé</p>
    </div>
    <div class="col-md-3 col-lg-3 col-xs-3 col-sm-3">
      <p style="background-color:rgba(255,255,0, 0.4)">Confirmé</p>
    </div>
    <div class="col-md-3 col-lg-3 col-xs-3 col-sm-3">
      <p class="planning-paye">Payé</p>
    </div>
    </div>
    </div>
    </div>

    </div>
    </section>
HTML;
      }

      return $content ;
  }



  public function render($i){
    switch($i){
      case 1 : $html = $this->afficherListeUtilisateurs();
        break;
      case 2 : $html = $this->afficherUtilisateur();
        break;
    }
    $v = new VuePageHTMLBootStrap($html);
    echo $v->showHTML();
  }

}
