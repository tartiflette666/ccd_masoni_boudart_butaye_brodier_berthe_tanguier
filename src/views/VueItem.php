<?php
namespace site\views;
use site\controllers\ControleurAvis;
use site\models\Avis;
use site\models\Item;
use site\models\User;
use \Slim\Slim;

/**
 *@class VueItem
 *
 *@package site\views
 */
class VueItem {
	/**
	* object Eloquent de l'item à afficher
	*/
	private $item;
	const AFFICHAGE_ITEM=1;
	const FORMULAIRE_ITEM=2;

    /**
     * Constructeur
     *@param $id id de l'item
     */
    public function __construct($id){
        $this->item=Item::find($id);
    }

    private function afficherCommentaires()
    {
        $racine = substr($_SERVER['SCRIPT_NAME'],0,strlen($_SERVER['SCRIPT_NAME'])-9);
        $html = "<h2>Commentaires</h2>";
        $arrayAvis = Avis::where('id_item', '=', $this->item['id'])->orderBy('date', 'DESC')->get();

        if(count($arrayAvis) === 0) {
            //$html .= "Pas de commentaire disponible pour cet item";
            $html .= <<<HTML
            <div class="alert alert-info">
              <strong>Information : </strong> Il n'y a pas de commentaire disponible pour cet item
            </div>
HTML;
        } else {
            foreach ($arrayAvis as $avis) {
                $user = User::select('nom', 'photo')->where('id', '=', $avis->id_user)->get();
                $dateFormat = new \DateTime($avis->date);
                $dateFormat = $dateFormat->format('d/m/Y');
                $html .= <<<HTML
                <div class="media">
                <div class="media-left">
HTML;

                if (file_exists('img/user/' . $user[0]["photo"]) && isset($user[0]["photo"])) {
                    $html .= <<<HTML
                <img src={$racine}img/user/{$user[0]["photo"]} class="media-object" style="width:100px">
HTML;
                } else {
                    $html .= <<<HTML
                <img src='{$racine}img/user/img_avatar1.png' class="media-object" style="width:100px">
HTML;
                }
                $html .= <<<HTML
  </div>
  <div class="media-body">
    <h4 class="media-heading">{$user[0]['nom']}</h4>
    <p>{$avis->commentaire}</p>
    <p><i>Le $dateFormat</i></p>
  </div>
</div>
HTML;
            }
        }
        return $html;
    }

    private function afficherFormulaireCreationCommentaire()
    {
        $app = Slim::getInstance();
        $html = "";
        if(isset($_SESSION['user']['nom'])) {
            if (User::retournerLevelUser($_SESSION['user']['nom']) > 0) {
                $html .= <<<HTML
<h2>Ajouter un commentaire</h2>
<form action={$app->urlFor('commentaireItem', array('id' => $this->item['id']))} method="post">
        <div class="form-group">
            <label for="message">Message</label>
            <input type="text" class="form-control" name="message" required>
        </div>
        <button type="submit" class="btn btn-default" name="valider_creation_commentaire">Valider</button>
</form>
HTML;
            }
        }
        return $html;
    }


	/**
	* @function render
	* @param aff parammetre d'affichage formulaire ou affichage simple
	*/
	public function render($aff){
		$content="";
		if($aff==self::AFFICHAGE_ITEM){
			$content=$this->afficherItem();
		}
		if($aff==self::FORMULAIRE_ITEM){
			$content=$this->afficherFormulaireReservationItem();
		}
		echo (new VuePageHTMLBootStrap($content))->showHTML();
	}


	public function afficherItem(){
		$app = Slim::getInstance();
		$i=$this->item;
		$root=$app->urlFor('root')."img/item/".$i->image;
		$content=<<<HTML
		<section class="container-fluid">
		<div class="container">
        <div class="row">
			<h1 class="titre_avant_separator">$i->nom</h1>
			<hr class="separator">
			<h2>Description</h2>
			<p>$i->description</p>
			<img src=$root style="width: 15em"></img>
			<br>
			<a href={$app->urlFor('planning',array('token'=>$i->token))}><button class="btn">Voir le planning</button></a>

			{$this->afficherFormulaireCreationCommentaire()}
			{$this->afficherCommentaires()}
		</div>
        </section>
HTML;
        return $content;
    }

    public function afficherFormulaireReservationItem(){
        $app=\Slim\Slim::getInstance();
        $i=$this->item;
        $content=<<<HTML
		<div class="container-fluid">
		<div class="container">
        <div class="row">
			<h1 class="titre_avant_separator">Reserver $i->nom</h1>
			<hr class="separator">
            <div class="form">
                <form action={$app->urlFor('validerReservation')} method="post">
                    <div class="form-group row">
							
							
                            <label for="jour" class="col-sm-1 col-form-label">Jour :</label>
							<div class="col-sm-2">
								<select name="jour" class="form-control form-control-sm">
								<option value="1" selected>Lundi</option>
								<option value="2">Mardi</option>
								<option value="3">Mercredi</option>
								<option value="4">Jeudi</option>
								<option value="5">Vendredi</option>
								</select>
							</div>
							
                            <label for="debut" class="col-sm-1 col-form-label">De :</label>
							<div class="col-sm-2">
								<select name="debut" class="form-control">
								<option value="8" selected>8h</option>
								<option value="10">10h</option>
								<option value="14">14h</option>
								<option value="16">16h</option>
								</select>
							</div>
							
							
                            <label for="debut" class="col-sm-1 col-form-label">Jusqu'à :</label>
							<div class="col-sm-2">
								<select name="fin" class="form-control form-control-sm">
								<option value="10" selected>10h</option>
								<option value="12">12h</option>
								<option value="16">16h</option>
								<option value="18">18h</option>
								</select>
							</div>
							<label for="debut" class="col-sm-1 col-form-label">Montant</label>
							<div class="col-sm-2">	
								
								<input type="number" class="form-control" name="montant">
							</div>	
							
							<label for="debut" class="col-sm-1 col-form-label">Cagnotte</label>
							<div class="col-sm-2">	
								
								<select name="cagnotte" class="form-control form-control-sm">
								<option value="0" selected>Non</option>
								<option value="1">Oui</option>
								</select>
							</div>
							<input type="hidden" name="id_item" value=$i->id>
					</div>
					
					<button type="submit" name="Valider" values="Valider" class="btn">Valider</button>
                </form>
            </div>
		</div>
		</div>
		</div>
        
HTML;
        return $content;
    }

    /**
     * @return string
     */
	public function formulaireModificationInformation($token){
	    $item=Item::findByToken($token);
	    $app=Slim::getInstance();
        $content = "";
	    if($item!=null) {
            $content .= <<<HTML
<div class="w3-row-padding w3-padding-64 w3-container">
      <div class="w3-content">
        <div class="w3-twothird">
              <h1 class="erreur">  </h1>
                  <h1 class="w3-padding-32">Modifier les informations de l'item</h1>
                  <form id="form1" method="POST" action="{$app->urlFor('valider_modification_item',array('token' => $token))}">
                    <label for="form1_id">Nom : </label>
                    </br>
                    <input required type="text" id="nom" name="nom" value="{$item->nom}" />
                    </br>
                    </br>
                    <label for="form1_id2">Description : </label>
                    </br>
                    <textarea style="width: 400px;height: 200px; resize:none;" required  id="form1_id2" name="description" value="{$item->description}" >{$item->description}</textarea>
                    </br>
                    </br>
                    <label for="form2_id3">Categorie </label>
                    </br>
                    <input required type="text" id="form1_id3" name="categorie" value="{$item->id_categ}"/>
                    </br>
                    
                    <button required class="w3-button w3-black w3-padding-large w3-large w3-margin-top" type="submit" name="valider_inscr" value="valid_f1">Valider</button>
                  </form>
                </div>
              </div>
            </div>
HTML;
        }

        echo (new VuePageHTMLBootStrap($content))->showHTML();

    }

}