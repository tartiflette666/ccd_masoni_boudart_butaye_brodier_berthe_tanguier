<?php
/**
 * Created by PhpStorm.
 * User: brodi
 * Date: 08/02/2018
 * Time: 18:29
 */

namespace site\controllers;

use site\models\Avis;
use site\models\Item;
use Slim\Slim;

class ControleurAvis
{
    public function ajoutCommentaire($id) {
        $texte = htmlspecialchars($_POST['message']) ;
        $idU = 0;
        if (isset($_SESSION['user'])) {
            $idU = $_SESSION['user']['id'];
        }
        $date = date('Y-m-d');

        $avis = new Avis();
        $avis->id_user = $idU;
        $avis->id_item = $id;
        $avis->commentaire = $texte;
        $avis->date = $date;
        $avis->save();

        $app = Slim::getInstance();
        $item = Item::select('token')->where('id', '=', $id)->first();
        $app->redirectTo('item', array('token' => $item['token']));
    }
}