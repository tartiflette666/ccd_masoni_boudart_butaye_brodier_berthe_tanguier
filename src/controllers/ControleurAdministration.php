<?php

namespace site\controllers;

use site\models\Categorie;
use site\models\Reservation;
use site\models\User;
use site\views\VueAdministrateur;
use Slim\Slim;

class ControleurAdministration
{
    /**
     * Vérifie si l'utilisateur est connecte et si celui-ci est un admin
     * @return bool
     */
    public function verifieSiConnecteEtEtreAdmin(){
        if(isset($_SESSION['user'])){
            $nom_user=$_SESSION['user'];
            $level=User::retournerLevelUser($nom_user->nom);
            if($level>=1){
                return true;
            }
        }
        return false;
    }

    /**
     * Aller au panneau de control de la vueAdministrateur
     */
    public function afficherLePanneauDeControl(){
        $app=Slim::getInstance();
        if($this->verifieSiConnecteEtEtreAdmin()){
            (new VueAdministrateur())->affichagePanneauDeControl();
        } else {
            //$_SESSION['erreur']="Vous n'avez pas les droits d'accès";
            //$app->redirectTo('home');
            unset($_SESSION['erreur']);
        }
    }

    /**
     *
     */
    public function choixDeConsultationDitem(){
        $app=Slim::getInstance();
        if($this->verifieSiConnecteEtEtreAdmin()){
            (new VueAdministrateur())->afficherChoixDeCategorieDitem();
        } else {
            //$_SESSION['erreur']="Vous n'avez pas les droits d'accès";
            //$app->redirectTo('home');
            unset($_SESSION['erreur']);
        }
    }

    /**
     *
     */
    public function afficherReservations(){
        $app=Slim::getInstance();
        if($this->verifieSiConnecteEtEtreAdmin()){
            (new VueAdministrateur())->afficherReservations();
        } else {
            //$_SESSION['erreur']="Vous n'avez pas les droits d'accès";
            //$app->redirectTo('home');
            unset($_SESSION['erreur']);
        }
    }

    /**
     *
     */
    public function confirmerReservations(){
        $app=Slim::getInstance();
        if($this->verifieSiConnecteEtEtreAdmin()){
            if(isset($_POST["choixReserv"])) {
                foreach ($_POST['choixReserv'] as $choix) {
                    $res = Reservation::where("id", "=", $choix)->first();
                    $res->etat = 'confirmé';
                    $res->save();
                }
                $app->redirectTo("afficher_reservations");
            }
        } else {
            //$_SESSION['erreur']="Vous n'avez pas les droits d'accès";
            //$app->redirectTo('home');
            unset($_SESSION['erreur']);
        }
    }

    public function annulerReservations(){
        $app=Slim::getInstance();
        if($this->verifieSiConnecteEtEtreAdmin()){
            if(isset($_POST["choixReserv"])) {
                foreach ($_POST['choixReserv'] as $choix) {
                    $res = Reservation::where("id", "=", $choix)->first();
                    $res->etat = 'annulé';
                    $res->save();
                }
                $app->redirectTo("afficher_reservations");
            }
        } else {
            //$_SESSION['erreur']="Vous n'avez pas les droits d'accès";
            //$app->redirectTo('home');
            unset($_SESSION['erreur']);
        }
    }

    /**
     *
     */
    public function afficherCreationCategories(){
        if($this->verifieSiConnecteEtEtreAdmin()){
            (new VueAdministrateur())->afficherCreationCategories();
        } else {
            //$_SESSION['erreur']="Vous n'avez pas les droits d'accès";
            //$app->redirectTo('home');
            unset($_SESSION['erreur']);
        }
    }

    public function creerCategorie(){
        $app=Slim::getInstance();
        if($this->verifieSiConnecteEtEtreAdmin()){
            if(isset($_POST["nomCat"]) && isset($_POST["descriptionCat"])) {
                $res = new Categorie();
                $res->nom = $_POST["nomCat"];
                $res->description = $_POST["descriptionCat"];
                $res->save();
                $app->redirectTo("root");
            }
        } else {
            $app->redirectTo("root");
            //$_SESSION['erreur']="Vous n'avez pas les droits d'accès";
            //$app->redirectTo('home');
            unset($_SESSION['erreur']);
        }
    }
}