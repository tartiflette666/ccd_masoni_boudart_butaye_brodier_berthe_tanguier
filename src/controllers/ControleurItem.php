<?php
namespace site\controllers;
use site\models\Item;
use site\models\Reservation;
use site\models\Options_reservation;
use \site\views\VueItem;
use site\models\User;
use site\models\Options;
use site\models\Cagnotte;
use Slim\Slim;


/**
* Class ControleurItem
*	Classe qui gère l'affichage des items et leur reservation
* @package site\controllers
*/
class ControleurItem
{
    /**
     *    fonction qui permet d'afficher un item et ses détails (liens vers formualaire reservation ect.)
     * @param $token
     */
    public function afficherItem($token)
    {
        $i = Item::findByToken($token);
        $v = new VueItem($i->id);
        $v->render(VueItem::AFFICHAGE_ITEM);
    }

    /**
     *
     * Crée l'objet de reservation et redirectionne vers connexion si l'utilisateur n'est pas connecté
     */
    public function validerReservation(){	
		$this->verifierPeriodeReservation($_POST['id_item'],$_POST['jour'],$_POST['debut'],$_POST['fin']);
        $app = \Slim\Slim::getInstance();
		$token=Item::find($_POST['id_item'])->token;
        if (!isset($_SESSION['user'])) {
            $app->redirectTo('verif_connexion');
        }
		$cagnotte = NULL;
		if($_POST['cagnotte']==1){
			$cagnotte=Cagnotte::max('id')+1;
			$c= new Cagnotte();
			$c->id=$cagnotte;
			$c->token=bin2hex(random_bytes(8));
			$c->save();
		}

        var_dump($_POST);
        var_dump($_SESSION);
        $r = new Reservation();
        $r->etat = "réservé";
        $r->date_reservation = $_POST['jour'];
        $r->heure_debut = $_POST['debut'];
        $r->heure_fin = $_POST['fin'];
        $r->date_creation = date("Y-m-d");
        $r->date_modif = date("Y-m-d");
        $r->montant = $_POST['montant'];
        $r->note = NULL;
        $r->id_user = $_SESSION['user']->id;
        $r->id_item = $_POST['id_item'];
        $r->id_cagnotte = $cagnotte;
        $r->save();

        $options = Options::all();
        foreach ($options as $o) {
          if(isset($_POST[$o->nom])){
            $o_r = new Options_reservation();
            $o_r->id_reservation = $r->id;
            $o_r->id_option = $o->id;
            $o_r->save();
          }
        }
       $app->redirectTo('planning',array('token'=>$token));			
    }

	/**
	* vérifie la validité du creneau horaire
	*
	*/
	public function verifierPeriodeReservation($idItem,$jour,$debut,$fin){
		$app=\Slim\Slim::getInstance();
		$token=Item::find($idItem)->token;
		$r1=Reservation::where('id_item')->where('date_reservation','=',$jour)->where('heure_debut','=',$debut)->first();
		$r2=Reservation::where('id_item')->where('date_reservation','=',$jour)->where('heure_fin','=',$fin)->first();
		$r3=Reservation::where('id_item')->where('date_reservation','=',$jour)->where('heure_fin','=',$fin)->first();
		if(isset($r1->id)||isset($r2->id)||isset($r3->id)){
			$app->redirectTo('planning',array('token'=>$token));
			$_SESSION['periodeReservationError']=1;	
		}else{
			unset($_SESSION['periodeReservationError']);
		}
		
		
	}
    /**
     * script de generation de token TEMPORAIRE !!
     *
     */
    public function genererTokens()
    {
        foreach (Item::all() as $i) {
            $i->token = bin2hex(random_bytes(8));
            $i->save();
        }
    }

    /**
     * Gere le formulaire de modification d'information d'item
     */
    public function modifierLesInformations($token)
    {
        $app=Slim::getInstance();
        if (isset($_SESSION['user'])) {
            if (User::retournerLevelUser($_SESSION['user']->nom) >= 1) {
                $v = new VueItem(Item::findByToken($token)->id);
                $v->formulaireModificationInformation($token);
            } else {
                $app->redirectTo('home');
            }
        } else {

            $app->redirectTo('home');
        }
    }

    /**
     * Traite les informations du formulaire de modification
     */
    public function gererFormulaireModification($token){
        $app=Slim::getInstance();
        if(isset($_POST['nom'])){
            $nom=filter_var($_POST['nom'],FILTER_SANITIZE_STRING);
        }
        if(isset($_POST['description'])){
            $description=filter_var($_POST['description'],FILTER_SANITIZE_STRING);
        }
        if(isset($_POST['categorie'])){
            $categorie=filter_var($_POST['categorie'],FILTER_SANITIZE_NUMBER_INT);
        }
        if(isset($nom) && isset($description) && isset($categorie)){
            Item::sauvegarderModifs($token,$nom,$description,$categorie);
        }
        $app->redirectTo('home');

    }
}
