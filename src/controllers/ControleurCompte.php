<?php

namespace site\controllers;

use site\models as m;
use site\views as v;
use Slim\Slim;

class ControleurCompte{

protected $app;

function __construct(){
  $this->app = Slim::getInstance();
}


  public function verifConnexion(){
    if(isset($_SESSION['user'])){
      $this->app->redirectTo("home");
    //  $this->app->redirect( $this->app->urlFor("home"));
    }
    else{
      $v = new v\VueCompte();
      $v->render(v\VueCompte::INSCRIPTION_CONNEXION);
    }
  }


  public function verifInscription(){
    if(isset($_SESSION['user'])){
      $this->app->redirectTo("home");
    //  $this->app->redirect( $this->app->urlFor("home"));
    }
    else{
      $v = new v\VueCompte();
      $v->render(v\VueCompte::INSCRIPTION_CONNEXION);
    }

}

    public function verifInscriptionConnexion() {
        if(isset($_SESSION['user'])){
            $this->app->redirectTo("home");
            //  $this->app->redirect( $this->app->urlFor("home"));
        }
        else{
            $v = new v\VueCompte();
            $v->render(v\VueCompte::INSCRIPTION_CONNEXION);
        }
    }

    public function verifModifCompte() {
        if(isset($_SESSION['user'])){
          $v = new v\VueCompte();
          $v->render(v\VueCompte::MODIF);
            //$this->app->redirect( $this->app->urlFor("home"));
        }
        else{
            $this->app->redirectTo("verif_connexion");
        }
    }


  public function verifAccesCompte(){

    if(isset($_SESSION['user'])){
      $v = new v\VueCompte();
      $v->render(v\VueCompte::COMPTE);

      //$this->app->redirect( $this->app->urlFor("compte"));
    }
    else{
      $this->app->redirectTo("verif_connexion");
    }
  }


  public function deconnexion(){
    // On détruit la session

    session_unset();
    session_destroy();

    $this->app->redirectTo("home");

  }


  public function validerConnexion(){
    if(isset($_POST)){
      // Si l'identifiant existe
      if ((  m\User::where("nom","=",filter_var($_POST['nom'] ,FILTER_SANITIZE_STRING))->count() ) > 0 ) {
        // On stocke le mot de passe
        $pwd = m\User::where("nom","=",filter_var($_POST['nom'] ,FILTER_SANITIZE_STRING))->first()->password;
        // Puis on effectue la vérification
        if (password_verify(filter_var($_POST['password'] ,FILTER_SANITIZE_STRING) , $pwd)) {
          // Si le mot de passe est bon, on affiche la bonne session
          $_SESSION['user']= m\User::where("nom","=",filter_var($_POST['nom'] ,FILTER_SANITIZE_STRING))->first();
          unset($_SESSION['erreurConnexion']);
          $this->app->redirectTo("home");
        }else{
          // Sinon on affiche de nouveau le formulaire de connexion avec un message d'erreur
          $_SESSION['erreurConnexion']="Mot de passe ou nom incorrect";
          $this->app->redirectTo("verif_connexion");
        }
      }else{
        $_SESSION['erreurConnexion']="Mot de passe ou nom incorrect";
        $this->app->redirectTo("verif_connexion");
      }
    }else{
      $this->app->redirectTo("verif_connexion");
    }
  }


  public function validerInscription(){

      if(isset($_POST)){
        if ($this->validationDonneeInscription() && $this->verifIdUnique()  && $this->verifMdpMeme() &&  $this->verifSecuMdp() && $this->verifEmailUnique() && $this->verifierLongueurEmail() && $this->verifierLongueurPassword()) {
      // On créer ensuite un User et on lui donne tous les paramètres
      $user=new m\User();
      $user->nom= filter_var($_POST['nom'] ,FILTER_SANITIZE_STRING);
      $user->email=filter_var($_POST['email'],FILTER_SANITIZE_EMAIL);
      $user->password=$this->crypterMdp(filter_var($_POST['password'] ,FILTER_SANITIZE_STRING));
      $user->token=bin2hex(random_bytes(8));

      // Puis on le sauvegarde dans la bdd
      $user->save();
      // Ensuite on sauvegarde la session
      $_SESSION['user']= m\User::where("nom","=",filter_var($_POST['nom'] ,FILTER_SANITIZE_STRING))->first();
      // Puis on affiche le compte de l'User
      unset($_SESSION['erreurInscription']);
      $this->app->redirectTo("home");
    }else{
      // Si les informations ne sont pas correctes on affiche un message d'erreur pour chaque erreur et on redirige vers la page d'inscription
      if(!$this->verifIdUnique()){
        $_SESSION['erreurInscription']="L'identifiant existe déjà.";
      }elseif (!$this->verifMdpMeme()) {
        $_SESSION['erreurInscription']="Mot de passe mal confirmé.";
      }elseif (!$this->verifSecuMdp()) {
        $_SESSION['erreurInscription']="Mot de passe trop simple, utilisez un mot de passe différent de votre identifiant qui possède au moins 6 caractères.";
      }elseif (!$this->verifEmailUnique()) {
        $_SESSION['erreurInscription']="Cet e-mail est déjà utilisé.";
      }elseif (!$this->verifierLongueurIdentifiant()) {
        $_SESSION['erreurInscription']="Identifiant trop grand.";
      }elseif (!$this->verifierLongueurNom()) {
        $_SESSION['erreurInscription']="Nom trop long.";
      }elseif (!$this->verifierLongueurPrenom()) {
        $_SESSION['erreurInscription']="Prenom trop long.";
      }elseif (!$this->verifierLongueurEmail()) {
        $_SESSION['erreurInscription']="Email trop grand.";
      }elseif (!$this->verifierLongueurPassword()) {
        $_SESSION['erreurInscription']="Mot de passe trop grand.";
      }elseif (!$this->validationDonneeInscription()) {
        $_SESSION['erreurInscription']="Données non valides.";
      }
      // Affichage de la page d'inscription
      $this->app->redirect($this->app->urlFor("verif_inscription"));
    }
  }else{
    $this->app->redirect($this->app->urlFor("verif_inscription"));
  }
}


public function validerModif(){
  if(isset($_SESSION)){
      // Si c'est le cas on regarde si post est défini
      if(isset($_POST)){


        // On regarde si les informations sont correctes
        if ( $this->validationDonneeInscription() && $this->verifAncienPassword() && $this->verifMdpMeme() &&  $this->verifSecuMdpNouveau() && $this->verifEmailUniqueModif() && $this->verifierLongueurEmail() && $this->verifierLongueurPassword()) {
          // On commence par récupèrer l'utilisateur
          $user= m\User::where("id","=",$_SESSION['user']->id)->first();
          // On modifie les données
          $user->password=$this->crypterMdp(filter_var($_POST['password'] ,FILTER_SANITIZE_STRING));
          $user->email=filter_var($_POST['email'],FILTER_SANITIZE_EMAIL);

          // Puis on sauvegarde
          $user->save();
          // Puis on affiche le compte de l'utilisateur

          $this->app->redirectTo("voir_compte");
        }else{
          // Si les informations ne sont pas correctes on affiche un message d'erreur pour chaque erreur et on redirige vers la page d'inscription
          if (!$this->verifMdpMeme()) {
            $_SESSION['erreur_modif']="Mot de passe mal confirmé.";
          }elseif (!$this->verifSecuMdpNouveau()) {
            $_SESSION['erreur_modif']="Mot de passe trop simple, utilisez un mot de passe différent de votre identifiant qui possède au moins 6 caractères.";
          }elseif (!$this->verifEmailUniqueModif()) {
            $_SESSION['erreur_modif']="Cet e-mail est déjà utilisé.";
          }elseif (!$this->verifierLongueurEmail()) {
            $_SESSION['erreur_modif']="Email trop grand.";
          }elseif (!$this->verifierLongueurPassword()) {
            $_SESSION['erreur_modif']="Mot de passe trop grand.";
          }elseif (!$this->validationDonneeInscription()) {
            $_SESSION['erreur_modif']="Données non valides.";
          }elseif (!$this->verifAncienPassword()){
            $_SESSION['erreur_modif']="Mauvais mot de passe.";
          }
          // Affichage de la page d'inscription

          $this->app->redirectTo("verif_modif_compte");
        }
      }else{
        // Si post n'est pas défini on redirige à l'accueil
        $this->app->redirectTo("home");
      }
    }else{
      // Si l'utilisateur n'est pas connecté on redirige à l'accueil
      $this->app->redirectTo("home");
    }
}



/**
  * Méthode de vérification de l'identifiant de la personne, si il existe déjà
  */
  private  function verifIdUnique(){
    $test= m\User::where("nom","=",filter_var($_POST['nom'] ,FILTER_SANITIZE_STRING))->count() ;
    return $test == 0;
  }

  /**
  * Vérification de l'unicité de l'adresse email
  */
  private  function verifEmailUnique(){
    $test= m\User::where("email","=",filter_var($_POST['email'],FILTER_SANITIZE_EMAIL))->count() ;
    return $test == 0;
  }

  /**
  * Vérification de l'unicité de l'adresse email quand l'User le change
  */
  private function verifEmailUniqueModif(){
    $test= m\User::where("email","=",filter_var($_POST['email'],FILTER_SANITIZE_EMAIL))->where("email","!=",$_SESSION['user']->email)->count() ;
    return $test == 0;
  }

  /**
  * Vérification de la sécurité du mot de passe
  */
  private function verifSecuMdp(){
    return ( strlen(filter_var($_POST['password'],FILTER_SANITIZE_STRING)) >=6 && filter_var($_POST['password'],FILTER_SANITIZE_STRING) != filter_var($_POST['nom'],FILTER_SANITIZE_STRING));
  }

  /**
  * Vérification de la sécurité du mot de passe lorsque l'User change de mot de passe
  */

  private function verifSecuMdpNouveau(){
    return ( strlen(filter_var($_POST['password'],FILTER_SANITIZE_STRING)) >=6 && filter_var($_POST['password'],FILTER_SANITIZE_STRING) != $_SESSION['user']->id);
  }

  /**
  * Cryptage du mot de passe
  */
  private function crypterMdp($s){
    return password_hash($s,PASSWORD_DEFAULT);
  }

  /**
  * Vérification de la confirmation du mot de passe
  */


  private function verifMdpMeme(){
    return filter_var($_POST['password'],FILTER_SANITIZE_STRING) == filter_var($_POST['confirmedpassword'],FILTER_SANITIZE_STRING);
  }

  /**
  * Vérification de la longueur de l'identifiant
  */
  private function verifierLongueurIdentifiant(){
    return (strlen(filter_var($_POST['nom'],FILTER_SANITIZE_STRING))<=60);
  }

  /**
  * Vérification de la longueur du mot de passe
  */
  private function verifierLongueurPassword(){
    return (strlen(filter_var($_POST['password'],FILTER_SANITIZE_STRING))<=60);
  }


  /**
  * Vérification de la longueur de l'email
  */
  private function verifierLongueurEmail(){
    return (strlen(filter_var($_POST['email'],FILTER_SANITIZE_EMAIL))<=60);
  }

  /**
  * Validation des données fournies par l'User lors de l'inscription
  */
  private function validationDonneeInscription(){
    return  filter_var( $_POST['email'], FILTER_VALIDATE_EMAIL);
  }

  /**
  * Méthode de vérification qui regarde si le password mis lors de la modification du compte est bon
  */
  private function verifAncienPassword(){

    return (password_verify(filter_var($_POST['old_password'],FILTER_SANITIZE_STRING) , m\User::where("id","=",$_SESSION['user']->id)->first()->password));
  }


  }
