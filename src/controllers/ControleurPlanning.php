<?php

namespace site\controllers;

use site\models\Item;
use site\models\Reservation;
use site\views\VuePlanning;
use Slim\Slim;

class ControleurPlanning{

  /*
  * fonction qui affiche le planning de resarvation d'un item
  */
  public function afficherPlanning($token){
    $item=Item::findByToken($token);
    //$item = Item::where('id', '=', $id)->first();
    $vue = new VuePlanning($item);
    $vue->render(1);
  }

  public function detailPlanning($id){
    $res = Reservation::where('id', '=', $id)->first();
    $vue = new VuePlanning($res);
    $vue->render(2);
  }

}
