<?php

namespace site\controllers;

use site\models\User;
use site\views\VueUtilisateur;
use Slim\Slim;

class ControleurUtilisateur{

  /*
  * fonction qui affiche la liste de tous les utilisateurs
  */
  public function afficherListeUtilisateurs(){
    $vue = new VueUtilisateur(null);
    $vue->render(1);
  }

  public function afficherUtilisateur($id){
    $user = User::where('id', '=', $id)->first();
    $vue = new VueUtilisateur($user);
    $vue->render(2);
  }

    /**
     * Formulaire de modification utilisateur
     * @param $id
     */
  public function modifierLesDroits($id){
        $app=Slim::getInstance();
        if(isset($_SESSION['user'])){
            if(User::retournerLevelUser($_SESSION['user']->nom) >= 1){
                if(isset($_POST['level'])){
                    $level=$_POST['level'];
                    User::modifierLevel($id,$level);
                    $app->redirectTo('home');
                }
            }
        } else {
          $app->redirectTo('home');
        }
  }

}
