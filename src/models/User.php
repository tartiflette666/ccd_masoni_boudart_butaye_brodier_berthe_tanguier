<?php

namespace site\models;
use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = "user_ccd";
    protected $primaryKey = "id";
    public $timestamps = false;

    public function reservations() {
        return $this->hasMany('site\models\Reservation','id_user')->get() ;
    }

    public function avis() {
        return $this->hasMany('site\models\Avis', 'id') ;
    }

    /**
     * Retoune le niveau de droit d'accès d'un utilisateur
     * @param $nom
     * @return mixed
     */
    public static function retournerLevelUser($nom){
        return User::select('level')->where('nom','=',$nom)->first()->level;
    }

    /**
     * Modifier Level d'un user
     * @param $id
     * @param $level
     */
    public static function modifierLevel($id,$level){
        $user=User::select('*')->where('id','=',$id)->first();
        if($user!=null){
            $user->level=$level;
            $user->save();
        }
    }
}
