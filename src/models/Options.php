<?php
/**
 * Created by PhpStorm.
 * User: berthe18u
 * Date: 08/02/2018
 * Time: 11:28
 */

namespace site\models;
use Illuminate\Database\Eloquent\Model;

class Options extends Model
{
    protected $table = "options_ccd";
    protected $primaryKey = "id";
    public $timestamps = false;

    /**
     * Permet de connaitre les items qui possèdent cette options
     * @param $id, id de l'option cherchée
     * @return array, tableau de tous les items concernés
     */
    public function item() {
        $items = Options_item::where('id_option', '=', $this->id)->get() ;
        $tabItems = array() ;
        foreach ($items as $i) {
            $tabItems[] = Item::where('id', '=', $i->id_item)->first() ;
        }
        return $tabItems ;
    }

}