<?php

namespace site\models;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $table = "item_ccd";
    protected $primaryKey = "id";
    public $timestamps = false;

    public static function ensembleDesItemsDuneCategorie(int $idCateg){
        $liste_des_items_de_la_categorie = array();
        $liste_de_tous_les_items=Item::select('*')->get();
        foreach($liste_de_tous_les_items as $item){
            $idCategDeItem=$item->id_categ;
            if($idCategDeItem == $idCateg){
                $liste_des_items_de_la_categorie[]=$item;
            }
        }
        return $liste_des_items_de_la_categorie;
    }
    /**
     * Permet de connaitre la categorie de l'item
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function categorie() {
        return $this->belongsTo('site\models\Categorie','id') ;
    }

    public function reservations() {
        return $this->hasMany('site\models\Reservation', 'id_item')->get() ;
    }

    /**
     * Permet de connaitre les options de l'item
     * @return array
     */
    public function options() {
        $table_options = Options_item::where('id_item', '=', $this->id)->get() ;
        $options = array() ;
        foreach ($table_options as $o) {
            $options[] = Options::where('id', '=', $o->id_option)->first() ;
        }
        return $options ;
    }

    public function avis() {
        return $this->hasMany('site\models\Avis', 'id') ;
    }

    /*
    public function verifHoraire($jour, $debut, $fin, $reservation ){

      $etat="libre";


      $start = $reservation->$heure_debut;
      $end = $reservation->$heure_fin;
      $day = $reservation->$jour;

      if ( ($day == $jour) && ()


      // retourner l 'état de l'item

    }

    */
	/***
	* @function findByToken
	*  @param token
	*  @return object eloquent
	*/
	public static function findByToken($token){
		return Item::where("token",'=',$token)->first();
	}

    /**
     * retourne le nombre d'item d'une catégorie
     */
	public static function retournerNombreDitem($idcateg){
	    $nbItem = Item::where('id_categ','=',$idcateg)->count('id');
	    return $nbItem;
    }

    /**
     * Sauvegarde les modifications du formulaire item
     * @param $token
     * @param $nom
     * @param $description
     * @param $id_categ
     */
    public static function sauvegarderModifs($token,$nom,$description,$id_categ){
        $item=Item::findByToken($token);
        if($item!=null) {
            $item->nom = $nom;
            $item->description = $description;
            $item->id_categ = $id_categ;
            $item->save();
        }
    }
}
