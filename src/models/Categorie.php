<?php

namespace site\models;
use Illuminate\Database\Eloquent\Model;

class Categorie extends Model
{
    protected $table = "categorie_ccd";
    protected $primaryKey = "id";
    public $timestamps = false;

    /**
     * Permet de connaitre les items de cette catégorie
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function items(){
        return $this->hasMany('site\models\Item', 'id_categ') ;
    }

    /**
     * Retourne le nom de la catégorie
     * @param $idcateg
     * @return mixed
     */
    public static function retournerNomDeLaCategorie($idcateg){
        return Categorie::select('nom')
    ->where('id','=',$idcateg)->first()->nom;
}
}