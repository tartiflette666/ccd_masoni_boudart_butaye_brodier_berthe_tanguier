<?php
/**
 * Created by PhpStorm.
 * User: berthe18u
 * Date: 08/02/2018
 * Time: 11:31
 */

namespace site\models;
use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    protected $table = "reservation_ccd";
    protected $primaryKey = "id";
    public $timestamps = false;

    public function user() {
        return $this->belongsTo('site\models\User', 'id') ;
    }

    public function item() {
        return $this->belongsTo('site\models\Item', 'id_item')->first() ;
    }

    public function cagnotte() {
        return $this->belongsTo('site\models\Cagnotte', 'id') ;
    }

    public function options() {
        return $this->hasMany('site\models\Options', 'id') ;
    }
}
