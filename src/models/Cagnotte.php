<?php
/**
 * Created by PhpStorm.
 * User: berthe18u
 * Date: 08/02/2018
 * Time: 11:27
 */

namespace site\models;
use Illuminate\Database\Eloquent\Model;

class Cagnotte extends Model
{
    protected $table = "cagnotte_ccd";
    protected $primaryKey = "id";
    public $timestamps = false;

    public function reservation() {
        return $this->belongsTo('site\models\Reservation', 'id') ;
    }
}