<?php
/**
 * Created by PhpStorm.
 * User: berthe18u
 * Date: 08/02/2018
 * Time: 11:29
 */

namespace site\models;
use Illuminate\Database\Eloquent\Model;

class Options_reservation extends Model
{
    protected $table = "options_reservation_ccd";
    protected $primaryKey = "id";

    public $timestamps = false;

}