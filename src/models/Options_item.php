<?php
/**
 * Created by PhpStorm.
 * User: berthe18u
 * Date: 08/02/2018
 * Time: 11:29
 */

namespace site\models;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Options_item. Permet de lister les options disponibles sur un item particuliers
 * @package site\models
 */
class Options_item extends Model
{
    protected $table = "options_item_ccd";
    protected $primaryKey = "id";
    public $timestamps = false;

}