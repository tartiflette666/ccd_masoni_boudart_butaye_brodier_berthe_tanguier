<?php
/**
 * Created by PhpStorm.
 * User: berthe18u
 * Date: 08/02/2018
 * Time: 12:29
 */

namespace site\models;
use Illuminate\Database\Eloquent\Model;

class Avis extends Model
{
    protected $table = "avis_ccd";
    protected $primaryKey = "id";
    public $timestamps = false;

    public function user() {
        return $this->belongsTo('site\models\User', 'id') ;
    }

    public function item() {
        return $this->belongsTo('site\models\Item', 'id') ;
    }

}