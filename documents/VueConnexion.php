<?php



class VueConnexion
{

    const CONNEXION = 0 ;
    const INSCRIPTION = 1 ;
    const INSCRIPTION_VALIDEE=2;
    const CONNEXION_VALIDEE=3;

    /**
     * /!\ IL FAUT AJOUTER UNE BALISE <p></p> DANS LE HTML POUR AFFICHER LE MESSAGE D'ERREUR
     * @var null
     */

    protected $errors=null;

    /**
     * VueConnexion constructor.
     * @param null $errors les erreurs de connexion. Si c'est null tout a bien fonctionné
     */
    function __construct($errors=null){
        if(isset($errors))
            $this->errors=$errors;
    }

    private function connect()
    {
        $app = Slim::getInstance();

        $style="" ;
        if(isset($this->errors)) {
            $style = "style='margin-top:3%'" ;
        }

        $content = <<<HTML
<div class="container">
    <div class="row">
        <div class="col-lg-8 mx-auto">
            <h1 class="text-center margin-top-12">Connexion</h1>
            <div class="margin-top-12" >
	            $this->errors
            </div>
            <div class="form form-connect" $style>
            <form action={$app->urlFor('valider_connexion')} method="post">
                    <label for="identifiant">Identifiant</label>
                    <input type="text" placeholder="john.doe@yahoo.fr" name="identifiant" required autofocus/>
                    <label for="MotDePasse">Mot de passe</label>
                    <input type="password" placeholder="••••••••" name ="password" required/>
                <input type="submit" value="Valider" name="Valider"/>
            </form>
            </div>
            <p class="text-center">Vous n'avez pas encore de compte ? <a href={$app->urlFor('inscription')}>Inscrivez-vous</a></p>
        </div>
    </div>
</div> 
HTML;
        return $content ;
    }

    private function subscribe()
    {
        $app = Slim::getInstance() ;
        $style=null ;
        if(isset($this->errors)) {
            $style = "style='margin-top:5%'" ;
        }
        $content = <<<HTML
<div class="container">
    <div class="row">
        <div class="col-lg-8 mx-auto">
	        <h1 class="text-center margin-top-12">Inscription</h1>
	        <div class="margin-top-12">
	            $this->errors
            </div>
		    <div class="form form-subscribe" $style>
		    
		    <form action={$app->urlFor('valider_inscription')} method="post">
		        <div class="row">
                    <div class="col-lg-6 col-md-12 text-center">
                        <label for="prenom">Prénom</label>
                        
                        <input type="text" name="prenom" placeholder="Titouan" required autofocus/> 
                        
                        <br>
                    </div>
                    <div class="col-lg-6 col-md-12 text-center">
                        <label for="nom">Nom</label>
                        
                        <input type="text" name="nom" placeholder="Doe"required autofocus/> 
                        
                        <br>
                    </div>
                 </div>
			    
			    <label for="dateNaiss">Date de naissance</label>
			    
			    <input type="date" name ="dateNaiss" required min="1900-01-01" max=date("YYYY-mm-dd")/>
			    
                <br>
            
                <label for="niveauNat">Niveau de natation</label>
                <select name="niveauNat">
                    <option value="debutant1">Débutant 1 : je sais flotter </option>
                    <option value="debutant2">Débutant 2 : je sais nager sur une courte distance (5m) </option>
                    <option value="confirme1">Confirmé 1 : je connais plusieurs nages </option>
                    <option value="confirme2">Confirmé 2 : je sais faire 25 mètres sans m'arrêter </option>
                    <option value="expert1">Expert 1 : je sais faire 25 mètres dans plusieurs nages </option>
                    <option value="expert2">Expert 2 : j'ai déjà participé à des compétitions </option>
                </select>
                <br><br>
                
                <label for="rue">Adresse</label>
                <div class="row">
                    <div class="col-lg-3 col-md-12 text-center">
                    
                        <input type="number" name ="num_adresse" placeholder="14" required/>
                        
                        <br>
                    </div>
                    <div class="col-lg-9 col-md-12 text-center">
                    
                        <input type="text" name ="rue_adresse" placeholder="route des mirabelliers"required/>
                        
                        <br>
                    </div>
                </div>
                <label for="ville">Ville</label>
                <div class="row">
                    <div class="col-lg-4 col-md-12 text-center">
                    
                        <input type="number" name ="codePostal" placeholder="54000"required/>
                        
                        <br>
                    </div>
                    <div class="col-lg-8 col-md-12 text-center">
                    
                        <input type="text" name ="ville_adresse" placeholder="Nancy" required/>
                        
                        <br>
                    </div>
                </div>
                
                
                <div class="row">
                    <div class="col-lg-6 col-md-12 text-center">
                        <label for="mail">Adresse mail</label>
                        
                        <input type="email" name ="mail" placeholder="john.doe@yahoo.fr" required/>
                        
                        <br>
                    </div>
                    <div class="col-lg-6 col-md-12 text-center">
                        <label for="tel">Numéro de téléphone</label>
                        
                        <input type="tel" name ="tel" placeholder="0678963468" required/>
                        
                        <br>
                     </div>
                 </div>
            
                <label for="MotDePasse">Mot de passe</label>
                <input type="password" name ="MotDePasse" placeholder="••••••••" required/>
                <br>
                <label for="MotDePasseValidation">Validation du mot de passe</label>
                <input type="password" name ="MotDePasseValidation" placeholder="••••••••" required/>
                <br>
				<input type="submit" value="Valider" name="Valider"/>
			</form>
			</div>
        </div>
    </div>
</div>
HTML;
        return $content ;
    }


    /**
     * Redirection après avoir validé son inscription
     * @return string
     */
    private function validation_inscription(){
        $app = Slim::getInstance() ;
        $this->errors = "<div class='alert alert-success'> Inscription validée. Veuillez vous connecter. </div>";
    return $this->connect() ;
    }

    /**
     * Redirection après avoir validé son inscription
     * @return string
     */
    /*private function validation_connexion(){
        $app = Slim::getInstance() ;
        $content = <<<HTML
        <!--<p>Connexion autorisée</p>-->
HTML;
        return $content;
    }*/

    /**
     * Rendu globale de la vue conenxion/inscription
     * @param $nb
     */
    public function render($nb) {
        switch($nb) {
            case VueConnexion::CONNEXION : $content = $this->connect() ;
            break ;
            case VueConnexion::INSCRIPTION : $content = $this->subscribe() ;
            break ;
            case VueConnexion::INSCRIPTION_VALIDEE : $content = $this->validation_inscription();
            break;
            /*case VueConnexion::CONNEXION_VALIDEE : $content = $this->validation_connexion();
                break;*/
        }
        $vue = new VuePageHTML($content) ;
        $vue->showHTML(true) ;
    }

}