<?php

//Lien pour voir les couleurs à utiliser
//https://color.adobe.com/fr/Th%C3%A8me-2-color-theme-8382848/

/**
 * Importation
 */
require 'vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as DB;
use \site\controllers as c;
use \site\views as v;

session_start();
/**
 * Base de données
 * Nommer son fichier de configuration de conenxion à la base de données : "dbconf.ini
 */
$db = new DB();
$db->addConnection(parse_ini_file('dbconf.ini'));
$db->setAsGlobal();
$db->bootEloquent();

/**
 * Application Slim
 */
$app = new \Slim\Slim;

/**
 * Routes
 */

$app->get('/',function (){
    (new v\VuePageHome())->home();
})->name('root');

$app->get('/home',function (){
    (new v\VuePageHome())->home();
})->name('home');

$app->get('/item/:token',function ($token){
    (new c\ControleurItem())->afficherItem($token);
})->name('item');

$app->post('/ajout_message/:id',function ($id){
    if(isset($_POST['valider_creation_commentaire']) && $_POST['valider_creation_commentaire']="commentaire_valide")
        (new c\ControleurAvis())->ajoutCommentaire($id);
})->name("commentaireItem");

$app->get('/genererTokens',function (){
    (new c\ControleurItem())->genererTokens();
})->name('genererTokens');

$app->get('/ateliers/:page',function ($page){
    (new v\VueCatalogue())->afficherLeCatalogueDesAteliers($page);
})->name("atelier");

$app->get('/voitures/:page',function ($page){
    (new v\VueCatalogue())->afficherLeCatalogueDesVoitures($page);
})->name("vehicule");


$app->post('/validerReservation', function(){
  (new c\ControleurItem())->validerReservation();
})->name("validerReservation");





/*
//Routes pour les formulaires de connexion et d'inscription
$app->get('/connexion', function(){
  (new v\VueCompte())->formulaire_connexion();
})->name('connexion');

$app->get('/inscription', function(){
  (new v\VueCompte())->formulaire_inscription();
})->name('inscription');

$app->get('/compte', function(){
  (new v\VueCompte())->afficher_compte();
})->name('compte');


*/



//Routes pour les controles de connexion et d'inscriptions
$app->get('/connexion', function(){
  (new c\ControleurCompte())->verifConnexion();
})->name("verif_connexion");

$app->get('/inscription', function(){
  (new c\ControleurCompte())->verifInscription();
})->name("verif_inscription");

$app->get('/inscription_connexion', function(){
    (new c\ControleurCompte())->verifInscriptionConnexion();
})->name("verif_inscription_connexion");

$app->get('/mon_compte/modif', function(){
    (new c\ControleurCompte())->verifModifCompte();
})->name("verif_modif_compte");


//Route pour verif l'accès au compte
$app->get('/mon_compte', function(){
  (new c\ControleurCompte())->verifAccesCompte();
})->name("voir_compte");


//Routes pour les vérifications sur formulaire
$app->post('/valider_connexion', function(){
  (new c\ControleurCompte())->validerConnexion();
})->name("valider_connexion");

$app->post('/valider_inscription', function(){
  (new c\ControleurCompte())->validerInscription();
})->name("valider_inscription");

$app->post('/mon_compte/valider_modif', function(){
  (new c\ControleurCompte())->validerModif();
})->name("valider_modif_compte");


//Route pour la déconnexion
$app->get('/deconnexion', function(){
  (new c\ControleurCompte())->deconnexion();
})->name("deconnexion");






//Route pour le planning
$app->get('/planning/:token',function ($token){
    (new c\ControleurPlanning())->afficherPlanning($token);
})->name('planning');

$app->get('/planningDetail/:id',function ($id){
    (new c\ControleurPlanning())->detailPlanning($id);
})->name('planningDetail');


//Route pour les utilisateurs
$app->get('/utilisateurs',function (){
    (new c\ControleurUtilisateur())->afficherListeUtilisateurs();
})->name('utilisateurs');

$app->get('/utilisateur/:id',function ($id){
    (new c\ControleurUtilisateur())->afficherUtilisateur($id);
})->name('utilisateur');

// Gestion espace administrateur
$app->get('/administration',function (){
    (new c\ControleurAdministration())->afficherLePanneauDeControl();
})->name('administration');

$app->get('/administration/choix_items',function (){
    (new site\controllers\ControleurAdministration())->choixDeConsultationDitem();
})->name('admin_choix_item');

$app->post('/administration/modification/utilisateur/modification_droit/:id',function ($id){
    (new c\ControleurUtilisateur())->modifierLesDroits($id);
})->name('modification_droit');

$app->get('/administration/modification/items/:token',function($token){
    (new c\ControleurItem())->modifierLesInformations($token);
})->name('modification_item');

$app->post('/administration/modification/items/validation_modification/:token',function($token){
    (new c\ControleurItem())->gererFormulaireModification($token);
})->name('valider_modification_item');

//   Gestion des réservations de tous les utilisateurs
$app->get('/afficher_reservations',function (){
    (new c\ControleurAdministration())->afficherReservations();
})->name('afficher_reservations');

$app->post('/confirmer_reservations',function (){
    if(isset($_POST['confirmer']) && $_POST['confirmer']="confirmer_fct")
        (new c\ControleurAdministration())->confirmerReservations();
    else if (isset($_POST['annuler']) && $_POST['annuler']="annuler_fct")
        (new c\ControleurAdministration())->annulerReservations();
})->name('confirmer_reservations');

//   Gestion des catégories en mode administrateur
$app->get('/afficher_creation_categories',function (){
    (new c\ControleurAdministration())->afficherCreationCategories();
})->name('afficher_creation_categories');

$app->post('/creer_categorie',function (){
    (new c\ControleurAdministration())->creerCategorie();
})->name('creer_categorie');

/**
 * Lancement de l'application Slim
 */
$app->run() ;
